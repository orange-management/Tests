<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\HumanResourceManagement\Models;

use Modules\HumanResourceManagement\Models\Employee;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class EmployeeTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $employee = new EmployeeMapper();

        $this->assertEquals(0, $employee->getId());
    }

    public function testSetGet()
    {
        $employee = new Employee();
        $account = new Account();

        $employee->setAccount($account);

        $this->assertEquals($account->getId(), $employee->getAccount()->getId());
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\HumanResourceManagement\Models;

use phpOMS\DataStorage\Database\Pool;
use Models\HumanResourceManagement\Models\Employee;
use Models\HumanResourceManagement\Models\EmployeeMapper;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class EmployeeMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new EmployeeMapper($this->dbPool->get());
        $accountMapper = new AccountMapper($this->dbPool->get());

        $employee = new Employee();
        $employee->setAccount($accountMapper->get());

        $id = $mapper->create($employee);
        $this->assertGreaterThan(0, $employee->getId());
        $this->assertEquals($id, $employee->getId());

        $employeeR = $mapper->get($employee->getId());
        $this->assertEquals($employee->getAccount()->getName1(), $employeeR->getAccount()->getName1());
    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume() 
    {
        $mapper = new EmployeeMapper($this->dbPool->get());
        $accountMapper = new AccountMapper($this->dbPool->get());

        $iEnd = mt_rand(20, 30);
        for ($i = 0; $i < $iEnd; $i++) {
            $account = $accountMapper->getRandom();
            $employee = new Employee();
            $employee->setAccount($account);

            $mapper->create($employee);
        }
    }
}

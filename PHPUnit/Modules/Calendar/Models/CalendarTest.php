<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Calendar\Models;

use Modules\Calendar\Models\Calendar;
use Modules\Calendar\Models\Event;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class CalendarTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $calendar = new Calendar();

        $this->assertEquals(0, $calendar->getId());
        $this->assertEquals(0, $calendar->getCreatedBy());
        $this->assertEquals('', $calendar->getName());
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $calendar->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals('', $calendar->getDescription());
        $this->assertEquals([], $calendar->getEvents());
        $this->assertInstanceOf('\Modules\Calendar\Models\NullEvent', $calendar->getEvent(0));
    }

    public function testSetGet()
    {
        $calendar = new Calendar();

        $calendar->setCreatedBy(1);
        $this->assertEquals(1, $calendar->getCreatedBy());

        $calendar->setCreatedAt($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $calendar->getCreatedAt()->format('Y-m-d'));

        $calendar->setName('Title');
        $this->assertEquals('Title', $calendar->getName());

        $calendar->setDescription('Description');
        $this->assertEquals('Description', $calendar->getDescription());

        $id = [];
        $id[] = $calendar->addEvent(new Event());
        $id[] = $calendar->addEvent(new Event());
        $success = $calendar->removeEvent(99);

        $this->assertFalse($success);

        $success = $calendar->removeEvent($id[1]);
        $this->assertTrue($success);

        $this->assertEquals(0, $calendar->getEvents()[0]->getId());
        $this->assertEquals(0, $calendar->getEvent(0)->getId());

        $this->assertInstanceOf('\Modules\Calendar\Models\Event', $calendar->getEvent(1));
    }
}

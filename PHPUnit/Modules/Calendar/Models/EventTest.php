<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Calendar\Models;

use Modules\Calendar\Models\Event;
use phpOMS\Account\Account;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class EventTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $event = new Event();

        $this->assertEquals(0, $event->getId());
        $this->assertEquals(0, $event->getCreatedBy());
        $this->assertEquals('', $event->getName());
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $event->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals('', $event->getDescription());
        $this->assertEquals([], $event->getPeople());
        $this->assertInstanceOf('\phpOMS\Account\NullAccount', $event->getPerson(1));
        $this->assertInstanceOf('\phpOMS\Datatypes\Location', $event->getLocation());
    }

    public function testSetGet()
    {
        $event = new Event();

        $event->setCreatedBy(1);
        $this->assertEquals(1, $event->getCreatedBy());

        $event->setName('Name');
        $this->assertEquals('Name', $event->getName());

        $event->setCreatedAt($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $event->getCreatedAt()->format('Y-m-d'));

        $event->setDescription('Description');
        $this->assertEquals('Description', $event->getDescription());

        $id = [];
        $id[] = $event->addPerson(new Account());
        $id[] = $event->addPerson(new Account());
        $success = $event->removePerson(99);

        $this->assertFalse($success);

        $success = $event->removePerson($id[1]);
        $this->assertTrue($success);

        $this->assertEquals(0, $event->getPeople()[0]->getId());
        $this->assertEquals(0, $event->getPerson(0)->getId());
    }
}

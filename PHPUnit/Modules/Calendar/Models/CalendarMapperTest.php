<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Calendar\Models;

use Modules\Calendar\Models\Calendar;
use Modules\Calendar\Models\CalendarMapper;
use Modules\Calendar\Models\Event;
use Modules\Calendar\Models\EventMapper;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Utils\RnG\Text;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class CalendarMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper   = new CalendarMapper($this->dbPool->get());
        $calendar = new Calendar();

        $calendar->setCreatedBy(1);
        $calendar->setCreatedAt($date = new \DateTime('2000-05-05'));
        $calendar->setName('Title');
        $calendar->setDescription('Description');

        $calendarEvent1 = new Event();
        $calendarEvent1->setName('Running test');
        $calendarEvent1->setDescription('Desc1');
        $calendarEvent1->setCreatedBy(1);
        $calendarEvent1->getSchedule()->setCreatedBy(1);
        $calendar->addEvent($calendarEvent1);

        $calendarEvent2 = new Event();
        $calendarEvent2->setName('Running test2');
        $calendarEvent2->setDescription('Desc2');
        $calendarEvent2->setCreatedBy(1);
        $calendarEvent2->getSchedule()->setCreatedBy(1);
        $calendar->addEvent($calendarEvent2);

        $id = $mapper->create($calendar);
        $this->assertGreaterThan(0, $calendar->getId());
        $this->assertEquals($id, $calendar->getId());

        $calendarR = $mapper->get($calendar->getId());
        $this->assertEquals($calendar->getCreatedAt()->format('Y-m-d'), $calendarR->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals($calendar->getCreatedBy(), $calendarR->getCreatedBy());
        $this->assertEquals($calendar->getDescription(), $calendarR->getDescription());
        $this->assertEquals($calendar->getName(), $calendarR->getName());

        $expected = $calendar->getEvents();
        $actual   = $calendarR->getEvents();
        $this->assertEquals(end($expected)->getDescription(), end($actual)->getDescription());
    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume()
    {
        // todo: foreach account
        $mapper = new CalendarMapper($this->dbPool->get());
        $eventMapper = new EventMapper($this->dbPool->get());

        $all = $mapper->getAll();

        foreach($all as $key => $calendar) {
            $iEnd = mt_rand(100, 1000);
            for ($i = 0; $i < $iEnd; $i++) {
                $lorem  = new Text();
                $lorem->setParagraphs(false);

                $event = new Event();
                $event->setName($lorem->generateText(mt_rand(3, 10)));
                $lorem->setParagraphs(true);
                $event->setDescription($lorem->generateText(mt_rand(20, 150)));
                $event->setCalendar($calendar->getId());
                $event->setCreatedBy(1);
                $event->getSchedule()->setCreatedBy(1);
                $event->setCreatedAt(new \DateTime(date("Y-m-d H:i:s", mt_rand(1356998400, (new \DateTime('now'))->getTimestamp()))));
                // when? =min - 1514764799

                $eventMapper->create($event);
            }
        }
    }
}

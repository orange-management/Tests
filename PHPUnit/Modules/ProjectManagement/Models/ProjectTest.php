<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\ProjectManagement\Models;

use Modules\ProjectManagement\Models\Project;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class ProjectTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $project = new Project();

        $this->assertEquals(0, $project->getId());
        $this->assertInstanceOf('\Modules\Calendar\Models\Calendar', $project->getCalendar());
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $project->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $project->getStart()->format('Y-m-d'));
        $this->assertEquals((new \DateTime('now'))->modifty('+1 month')->format('Y-m-d'), $project->getEnd()->format('Y-m-d'));
        $this->assertEquals(0, $project->getCreatedBy());
        $this->assertEquals('', $project->getName());
        $this->assertEquals('', $project->getDescription());
        $this->assertEquals(0, $project->getCosts()->getInt());
        $this->assertEquals(0, $project->getBudget()->getInt());
        $this->assertEquals(0, $project->getEarnigns()->getInt());
        $this->assertEmpty($project->getTasks());
        $this->assertFalse($project->removeTask(2));
        $this->assertEmpty('\Modules\Tasks\Models\NullTask', $project->getTask(0));
    }

    public function testSetGet()
    {
        $project = new Project();

        $project->setName('Project');
        $this->assertEquals('Project', $project->getName());

        $project->setDescription('Description');
        $this->assertEquals('Description', $project->getDescription());

        $project->setCreatedAt($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $project->getCreatedAt()->format('Y-m-d'));

        $project->setStart($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $project->getStart()->format('Y-m-d'));

        $project->setEnd($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $project->getEnd()->format('Y-m-d'));

        $money = new Money();
        $money->setString('1.23');

        $project->setCosts($money);
        $this->assertEquals($money->getAmount(), $project->getCosts()->getAmount());

        $project->setBudget($money);
        $this->assertEquals($money->getAmount(), $project->getBudget()->getAmount());

        $project->setEarnings($money);
        $this->assertEquals($money->getAmount(), $project->getEarnings()->getAmount());

        $task = new Task();
        $task->setTitle('A');

        $this->assertTrue($project->removeTask(0));
        $this->assertEquals('A', $project->getTask(0)->getTitle());

        $this->removeTask(0);
        $this->assertEquals(0, $project->countTasks());

        $project->addTask($task);
        $this->assertEquals(1, count($project->getTasks()));
    }
}

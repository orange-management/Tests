<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\ProjectManagement\Models;

use Modules\ProjectManagement\Models\Project;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Utils\RnG\DateTime;
use phpOMS\Utils\RnG\Text;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class ProjectMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new ProjectMapper($this->dbPool->get());

        $project = new Project();

        $project->setName('Projectname');
        $project->setDescription('Description');
        $project->setCreatedAt($date = new \DateTime('2000-05-05'));
        $project->setStart($date = new \DateTime('2000-05-05'));
        $project->setEnd($date = new \DateTime('2000-05-05'));

        $money = new Money();
        $money->setString('1.23');

        $project->setCosts($money);
        $project->setBudget($money);
        $project->setEarnings($money);

        $task = new Task();
        $task->setTitle('A');

        $task2 = new Task();
        $task2->setTitle('B');

        $project->addTask($task);
        $project->addTask($task2);

        $id = $mapper->create($project);
        $this->assertGreaterThan(0, $project->getId());
        $this->assertEquals($id, $project->getId());

        $projectR = $mapper->get($project->getId());

        $this->assertEquals($project->getName(), $projectR->getName());
        $this->assertEquals($project->getDescription(), $projectR->getDescription());
        $this->assertEquals($project->countTasks(), $projectR->countTasks());
        $this->assertEquals($project->getCosts()->getAmount(), $projectR->getCosts()->getAmount());
        $this->assertEquals($project->getBudget()->getAmount(), $projectR->getBudget()->getAmount());
        $this->assertEquals($project->getEarnings()->getAmount(), $projectR->getEarnings()->getAmount());
        $this->assertEquals($project->getCreatedAt->format('Y-m-d'), $projectR->getCreatedAt->format('Y-m-d'));
        $this->assertEquals($project->getStart->format('Y-m-d'), $projectR->getStart->format('Y-m-d'));
        $this->assertEquals($project->getEnd->format('Y-m-d'), $projectR->getEnd->format('Y-m-d'));
        
    }

    public function testNewest()
    {
        $mapper = new ProjectMapper($this->dbPool->get());
        $newest = $mapper->getNewest(1);

        $this->assertEquals(1, count($newest));
    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume()
    {

        // todo: foreach account
        $mapper = new ProjectMapper($this->dbPool->get());

        $iEnd = mt_rand(20, 300);
        for ($i = 0; $i < $iEnd; $i++) {
            $lorem = new Text();
            $lorem->setParagraphs(false);

            $project = new Project();

            $project->setType(ProjectType::getRandom());
            $project->setName();

            $project->setName($lorem->generateText(mt_rand(3, 10)));

            $lorem->setParagraphs(true);
            $project->setDescription($lorem->generateText(mt_rand(20, 50)));
            $project->setCreatedAt($date = new \DateTime('2000-05-05'));
            $project->setStart($date = new \DateTime('2000-05-05'));
            $project->setEnd($date = new \DateTime('2000-05-05'));

            $money = new Money();
            $money->setInt(mt_rand(PHP_INT_MIN, PHP_INT_MAX));
            $project->setCosts($money);

            $money = new Money();
            $money->setInt(mt_rand(PHP_INT_MIN, PHP_INT_MAX));
            $project->setBudget($money);

            $money = new Money();
            $money->setInt(mt_rand(PHP_INT_MIN, PHP_INT_MAX));
            $project->setEarnings($money);

            $lorem->setParagraphs(false);
            
            $iEnd2 = mt_rand(0, 50);
            for ($j = 0; $i < $iEnd2; $j++) {
                $task = new Task();
                $task->setTitle($lorem->generateText(mt_rand(3, 10)));

                $project->addTask($task);
            }

            $mapper->create($project);
        }
    }
}

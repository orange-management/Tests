<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Media;

use Modules\Media\Models\UploadStatus;
use phpOMS\ApplicationAbstract;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Module\ModuleFactory;
use phpOMS\Router\Router;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../config.php';

class ControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $app    = null;
    protected $module = null;

    protected function setUp()
    {
        $this->app = new class extends ApplicationAbstract
        {
        };

        $this->app->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->app->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);

        $this->app->router = new Router();

        $this->module = ModuleFactory::getInstance('Media', $this->app);
    }

    public function testCreateDbEntries()
    {
        $status = [
            [
                'status' => UploadStatus::OK,
                'extension' => 'png',
                'filename' => 'logo.png',
                'path' => 'Tests/PHPUnit/Modules/Media/Files/logo.png',
                'size' => 90210,
            ],
            [
                'status' => UploadStatus::FAILED_HASHING,
                'extension' => 'png',
                'filename' => 'logo.png',
                'path' => 'Tests/PHPUnit/Modules/Media/Files/logo.png',
                'size' => 90210,
            ],
            [
                'status' => UploadStatus::OK,
                'extension' => 'png',
                'filename' => 'logo2.png',
                'path' => 'Tests/PHPUnit/Modules/Media/Files/logo2.png',
                'size' => 90210,
            ],
        ];

        $ids = $this->module->createDbEntries($status, 1);
        $this->assertEquals(2, count($ids));
    }

}

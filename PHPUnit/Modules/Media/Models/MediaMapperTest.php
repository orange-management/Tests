<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Media\Models;

use Modules\Media\Models\Media;
use Modules\Media\Models\MediaMapper;
use phpOMS\DataStorage\Database\Pool;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class MediaMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new MediaMapper($this->dbPool->get());

        $media = new Media();
        $media->setCreatedAt($data = new \DateTime('now'));
        $media->setCreatedBy(1);
        $media->setDescription('desc');
        $media->setPath('some/path');
        $media->setSize(11);
        $media->setExtension('png');
        $media->setName('Image');
        $id = $mapper->create($media);

        $this->assertGreaterThan(0, $media->getId());
        $this->assertEquals($id, $media->getId());

        $mediaR = $mapper->get($media->getId());
        $this->assertEquals($data->format('Y-m-d'), $mediaR->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals($media->getCreatedBy(), $mediaR->getCreatedBy());
        $this->assertEquals($media->getDescription(), $mediaR->getDescription());
        $this->assertEquals($media->getPath(), $mediaR->getPath());
        $this->assertEquals($media->getSize(), $mediaR->getSize());
        $this->assertEquals($media->getExtension(), $mediaR->getExtension());
        $this->assertEquals($media->getName(), $mediaR->getName());
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Media\Models;

use Modules\Media\Models\Collection;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class CollectionTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $media = new Collection();


        $this->assertEquals(0, $media->getId());
        $this->assertEquals(0, $media->getCreatedBy());
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $media->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals('collection', $media->getExtension());
        $this->assertEquals('', $media->getPath());
        $this->assertEquals('', $media->getName());
        $this->assertEquals('', $media->getDescription());
        $this->assertEquals(0, $media->getSize());
        $this->assertEquals(false, $media->isVersioned());
        $this->assertEquals([], $media->getSources());
    }

    public function testSetGet()
    {
        $media = new Collection();

        $media->setCreatedBy(1);
        $this->assertEquals(1, $media->getCreatedBy());

        $media->setCreatedAt($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $media->getCreatedAt()->format('Y-m-d'));

        $media->setExtension('pdf');
        $this->assertEquals('collection', $media->getExtension());

        $media->setPath('/home/root');
        $this->assertEquals('/home/root', $media->getPath());

        $media->setName('Report');
        $this->assertEquals('Report', $media->getName());

        $media->setDescription('This is a description');
        $this->assertEquals('This is a description', $media->getDescription());

        $media->setSize(11);
        $this->assertEquals(11, $media->getSize());

        $media->setVersioned(true);
        $this->assertEquals(false, $media->isVersioned());

        $media->setSources([1, 2, 3]);
        $this->assertEquals([1, 2, 3], $media->getSources());

        $media->addSource(4);
        $this->assertEquals([1, 2, 3, 4], $media->getSources());
    }
}

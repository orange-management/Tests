<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Organization\Models;

use Modules\Organization\Models\Unit;
use Modules\Organization\Models\UnitMapper;
use phpOMS\DataStorage\Database\Pool;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class UnitMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new UnitMapper($this->dbPool->get());

        $unit = new Unit();
        $unit->setName('Scrooge Inc.');
        $unit->setDescription('Description');
        $unit->setParent(1);

        $id = $mapper->create($unit);

        $unitR = $mapper->get(2);
        $this->assertEquals($id, $unitR->getId());
        $this->assertEquals($unit->getName(), $unitR->getName());
        $this->assertEquals($unit->getDescription(), $unitR->getDescription());
        $this->assertEquals($unit->getParent(), $unitR->getParent());
    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume()
    {
        $mapper = new UnitMapper($this->dbPool->get());

        /* 2 */
        $unit = new Unit();
        $unit->setName('Flintheart Inc.');
        $unit->setDescription('Description');
        $unit->setParent(1);
        $mapper->create($unit);
    }
}

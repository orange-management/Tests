<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Organization\Models;

use Modules\Organization\Models\Department;
use Modules\Organization\Models\DepartmentMapper;
use phpOMS\DataStorage\Database\Pool;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class DepartmentMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new DepartmentMapper($this->dbPool->get());

        $department = new Department();
        $department->setName('Management');
        $department->setDescription('Description');
        $department->setUnit(1);

        $id = $mapper->create($department);

        $departmentR = $mapper->get(1);
        $this->assertEquals($id, $departmentR->getId());
        $this->assertEquals($department->getName(), $departmentR->getName());
        $this->assertEquals($department->getDescription(), $departmentR->getDescription());
        $this->assertEquals($department->getParent(), $departmentR->getParent());
        $this->assertEquals($department->getUnit(), $departmentR->getUnit());

    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume()
    {
        $mapper = new DepartmentMapper($this->dbPool->get());

        /* 2 */
        $department = new Department();
        $department->setName('HR');
        $department->setDescription('Description');
        $department->setParent(1);
        $department->setUnit(1);
        $mapper->create($department);

        /* 3 */
        $department = new Department();
        $department->setName('QM');
        $department->setDescription('Description');
        $department->setParent(1);
        $department->setUnit(1);
        $mapper->create($department);

        /* 4 */
        $department = new Department();
        $department->setName('Sales');
        $department->setDescription('Description');
        $department->setParent(1);
        $department->setUnit(1);
        $mapper->create($department);

        /* 5 */
        $department = new Department();
        $department->setName('Shipping');
        $department->setDescription('Description');
        $department->setParent(4);
        $department->setUnit(1);
        $mapper->create($department);

        /* 6 */
        $department = new Department();
        $department->setName('Purchase');
        $department->setDescription('Description');
        $department->setParent(1);
        $department->setUnit(1);
        $mapper->create($department);

        /* 7 */
        $department = new Department();
        $department->setName('Arrival');
        $department->setDescription('Description');
        $department->setParent(6);
        $department->setUnit(1);
        $mapper->create($department);

        /* 8 */
        $department = new Department();
        $department->setName('Accounting');
        $department->setDescription('Description');
        $department->setParent(1);
        $department->setUnit(1);
        $mapper->create($department);

        /* 9 */
        $department = new Department();
        $department->setName('Production');
        $department->setDescription('Description');
        $department->setParent(1);
        $department->setUnit(1);
        $mapper->create($department);

        /* 10 */
        $department = new Department();
        $department->setName('Marketing');
        $department->setDescription('Description');
        $department->setParent(1);
        $department->setUnit(1);
        $mapper->create($department);
    }
}

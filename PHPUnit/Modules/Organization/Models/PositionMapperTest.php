<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Organization\Models;

use Modules\Organization\Models\Position;
use Modules\Organization\Models\PositionMapper;
use phpOMS\DataStorage\Database\Pool;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class PositionMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new PositionMapper($this->dbPool->get());

        $position = new Position();
        $position->setName('CEO');
        $position->setDescription('Description');

        $id = $mapper->create($position);

        $positionR = $mapper->get(1);
        $this->assertEquals($id, $positionR->getId());
        $this->assertEquals($position->getName(), $positionR->getName());
        $this->assertEquals($position->getDescription(), $positionR->getDescription());
        $this->assertEquals($position->getParent(), $positionR->getParent());
    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume()
    {
        $mapper = new PositionMapper($this->dbPool->get());

        /* 2 */
        $position = new Position();
        $position->setName('CFO');
        $position->setDescription('Description');
        $position->setParent(1);
        $mapper->create($position);

        /* 3 */
        $position = new Position();
        $position->setName('Accountant');
        $position->setDescription('Description');
        $position->setParent(2);
        $mapper->create($position);

        /* 4 */
        $position = new Position();
        $position->setName('Controller');
        $position->setDescription('Description');
        $position->setParent(3);
        $mapper->create($position);

        /* 5 */
        $position = new Position();
        $position->setName('Sales Director');
        $position->setDescription('Description');
        $position->setParent(1);
        $mapper->create($position);

        /* 6 */
        $position = new Position();
        $position->setName('Purchase Director');
        $position->setDescription('Description');
        $position->setParent(1);
        $mapper->create($position);

        /* 7 */
        $position = new Position();
        $position->setName('Territory Manager');
        $position->setDescription('Description');
        $position->setParent(5);
        $mapper->create($position);

        /* 8 */
        $position = new Position();
        $position->setName('Territory Sales Assistant');
        $position->setDescription('Description');
        $position->setParent(7);
        $mapper->create($position);

        /* 9 */
        $position = new Position();
        $position->setName('Domestic Sales Manager');
        $position->setDescription('Description');
        $position->setParent(5);
        $mapper->create($position);
    }
}

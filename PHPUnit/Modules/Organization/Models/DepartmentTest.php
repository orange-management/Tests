<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Organization\Models;

use Modules\Organization\Models\Department;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class DepartmentTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $department = new Department();

        $this->assertEquals(0, $department->getId());
        $this->assertEquals('', $department->getName());
        $this->assertEquals('', $department->getDescription());
        $this->assertEquals(null, $department->getParent());
        $this->assertEquals(1, $department->getUnit());
    }

    public function testSetGet()
    {
        $department = new Department();

        $department->setName('Name');
        $this->assertEquals('Name', $department->getName());

        $department->setDescription('Description');
        $this->assertEquals('Description', $department->getDescription());

        $department->setParent(1);
        $this->assertEquals(1, $department->getParent());

        $department->setUnit(1);
        $this->assertEquals(1, $department->getUnit());
    }
}

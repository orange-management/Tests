<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Organization\Models;

use Modules\Organization\Models\Position;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class PositionTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $position = new Position();

        $this->assertEquals(0, $position->getId());
        $this->assertEquals('', $position->getName());
        $this->assertEquals('', $position->getDescription());
        $this->assertEquals(null, $position->getParent());
    }

    public function testSetGet()
    {
        $position = new Position();

        $position->setName('Name');
        $this->assertEquals('Name', $position->getName());

        $position->setDescription('Description');
        $this->assertEquals('Description', $position->getDescription());

        $position->setParent(1);
        $this->assertEquals(1, $position->getParent());
    }
}

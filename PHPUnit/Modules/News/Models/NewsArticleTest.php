<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Tasks\Models;

use Modules\News\Models\NewsArticle;
use Modules\News\Models\NewsStatus;
use Modules\News\Models\NewsType;
use phpOMS\Localization\ISO639x1Enum;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class NewsArticleTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $news = new NewsArticle();

        $this->assertEquals(0, $news->getId());
        $this->assertEquals(0, $news->getCreatedBy());
        $this->assertEquals('', $news->getTitle());
        $this->assertEquals('', $news->getContent());
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $news->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $news->getPublish()->format('Y-m-d'));
        $this->assertFalse($news->isFeatured());
        $this->assertEquals(ISO639x1Enum::_EN, $news->getLanguage());
        $this->assertEquals(NewsStatus::DRAFT, $news->getStatus());
        $this->assertEquals(NewsType::ARTICLE, $news->getType());
    }

    public function testSetGet()
    {
        $news = new NewsArticle();

        $news->setCreatedBy(1);
        $this->assertEquals(1, $news->getCreatedBy());

        $news->setTitle('Title');
        $this->assertEquals('Title', $news->getTitle());

        $news->setContent('Content');
        $this->assertEquals('Content', $news->getContent());

        $news->setCreatedAt($data = new \DateTime('2001-05-05'));
        $this->assertEquals($data, $news->getCreatedAt());

        $news->setPublish($data = new \DateTime('2001-05-07'));
        $this->assertEquals($data, $news->getPublish());

        $news->setFeatured(true);
        $this->assertTrue($news->isFeatured());

        $news->setLanguage(ISO639x1Enum::_DE);
        $this->assertEquals(ISO639x1Enum::_DE, $news->getLanguage());

        $news->setStatus(NewsStatus::VISIBLE);
        $this->assertEquals(NewsStatus::VISIBLE, $news->getStatus());

        $news->setType(NEwsType::HEADLINE);
        $this->assertEquals(NewsType::HEADLINE, $news->getType());
    }
}

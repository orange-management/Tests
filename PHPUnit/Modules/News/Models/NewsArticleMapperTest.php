<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Tasks\Models;

use Modules\News\Models\NewsArticle;
use Modules\News\Models\NewsArticleMapper;
use Modules\News\Models\NewsStatus;
use Modules\News\Models\NewsType;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Localization\ISO639x1Enum;
use phpOMS\Utils\RnG\Text;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class NewsArticleMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new NewsArticleMapper($this->dbPool->get());
        $news   = new NewsArticle();

        $news->setCreatedBy(1);
        $news->setTitle('Title');
        $news->setContent('Content');
        $news->setCreatedAt(new \DateTime('2001-05-05'));
        $news->setPublish(new \DateTime('2001-05-07'));
        $news->setFeatured(true);
        $news->setLanguage(ISO639x1Enum::_DE);
        $news->setStatus(NewsStatus::VISIBLE);
        $news->setType(NewsType::HEADLINE);

        $id = $mapper->create($news);
        $this->assertGreaterThan(0, $news->getId());
        $this->assertEquals($id, $news->getId());

        $newsR = $mapper->get($news->getId());
        $this->assertEquals($news->getCreatedAt()->format('Y-m-d'), $newsR->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals($news->getCreatedBy(), $newsR->getCreatedBy());
        $this->assertEquals($news->getContent(), $newsR->getContent());
        $this->assertEquals($news->getTitle(), $newsR->getTitle());
        $this->assertEquals($news->getStatus(), $newsR->getStatus());
        $this->assertEquals($news->getType(), $newsR->getType());
        $this->assertEquals($news->getLanguage(), $newsR->getLanguage());
        $this->assertEquals($news->isFeatured(), $newsR->isFeatured());
        $this->assertEquals($news->getPublish()->format('Y-m-d'), $newsR->getPublish()->format('Y-m-d'));
    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume()
    {
        // todo: foreach account
        $mapper = new NewsArticleMapper($this->dbPool->get());

        $iEnd = mt_rand(100, 300);
        for ($i = 0; $i < $iEnd; $i++) {
            $lorem = new Text();
            $lorem->setParagraphs(false);
            $news = new NewsArticle();

            $news->setCreatedBy(1);
            $news->setCreatedAt(new \DateTime(date("Y-m-d H:i:s", mt_rand(1356998400, (new \DateTime('now'))->getTimestamp()))));
            $news->setTitle($lorem->generateText(mt_rand(3, 10)));

            if (mt_rand(1, 10) < 8) {
                $status = NewsStatus::VISIBLE;
            } else {
                $status = NewsStatus::DRAFT;
            }

            $news->setStatus($status);

            if (($rand = mt_rand(1, 10)) < 5) {
                $type = NewsType::ARTICLE;
            } elseif ($rand < 8) {
                $type = NewsType::HEADLINE;
            } else {
                $type = NewsType::LINK;
            }

            $news->setType($type);

            if(mt_rand(1, 10) < 3) {
                $news->setFeatured(true);
            } else {
                $news->setFeatured(false);
            }

            $news->setPublish(new \DateTime(date("Y-m-d H:i:s", mt_rand($news->getCreatedAt()->getTimestamp(), (new \DateTime('now'))->getTimestamp()+2592000))));

            $lorem->setParagraphs(true);

            if($type === NewsType::LINK) {
                $news->setContent('http://www.google.com');
            } else {
                $news->setContent($lorem->generateText(mt_rand(50, 500)));
            }

            $mapper->create($news);
        }
    }
}

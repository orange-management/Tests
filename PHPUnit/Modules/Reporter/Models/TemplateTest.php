<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Reporter\Models;

use Modules\Reporter\Models\ReporterStatus;
use Modules\Reporter\Models\Template;
use Modules\Reporter\Models\TemplateDataType;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class TemplateTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $template = new Template();

        $this->assertEquals(0, $template->getId());
        $this->assertEquals(0, $template->getCreatedBy());
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $template->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals('', $template->getName());
        $this->assertEquals(ReporterStatus::INACTIVE, $template->getStatus());
        $this->assertEquals('', $template->getDescription());
        $this->assertEquals([], $template->getExpected());
        $this->assertEquals(0, $template->getSource());
        $this->assertEquals(TemplateDataType::OTHER, $template->getDatatype());
    }

    public function testSetGet()
    {
        $template = new Template();

        $template->setCreatedBy(1);
        $this->assertEquals(1, $template->getCreatedBy());

        $template->setCreatedAt($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $template->getCreatedAt()->format('Y-m-d'));

        $template->setName('Title');
        $this->assertEquals('Title', $template->getName());

        $template->setStatus(ReporterStatus::ACTIVE);
        $this->assertEquals(ReporterStatus::ACTIVE, $template->getStatus());

        $template->setDescription('Description');
        $this->assertEquals('Description', $template->getDescription());

        $template->setExpected(['source1.csv', 'source2.csv']);
        $this->assertEquals(['source1.csv', 'source2.csv'], $template->getExpected());

        $template->setSource(4);
        $this->assertEquals(4, $template->getSource());

        $template->setDatatype(TemplateDataType::GLOBAL_DB);
        $this->assertEquals(TemplateDataType::GLOBAL_DB, $template->getDatatype());
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Reporter\Models;

use Modules\Reporter\Models\Report;
use Modules\Reporter\Models\ReporterStatus;
use Modules\Reporter\Models\ReportMapper;
use phpOMS\DataStorage\Database\Pool;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class ReportMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new ReportMapper($this->dbPool->get());

        $report = new Report();

        $report->setCreatedBy(1);
        $report->setCreatedAt($date = new \DateTime('2000-05-05'));
        $report->setTitle('Title');
        $report->setStatus(ReporterStatus::ACTIVE);
        $report->setDescription('Description');
        $report->setTemplate(2);
        $report->setSource(15);

        $id = $mapper->create($report);
        $this->assertGreaterThan(0, $report->getId());
        $this->assertEquals($id, $report->getId());

        $reportR = $mapper->get($report->getId());
        $this->assertEquals($date->format('Y-m-d'), $reportR->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals($report->getCreatedBy(), $reportR->getCreatedBy());
        $this->assertEquals($report->getDescription(), $reportR->getDescription());
        $this->assertEquals($report->getTitle(), $reportR->getTitle());
        $this->assertEquals($report->getStatus(), $reportR->getStatus());
        $this->assertEquals($report->getTemplate(), $reportR->getTemplate());
    }
}

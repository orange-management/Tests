<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Reporter\Models;

use Modules\Reporter\Models\ReporterStatus;
use Modules\Reporter\Models\Template;
use Modules\Reporter\Models\TemplateDataType;
use Modules\Reporter\Models\TemplateMapper;
use phpOMS\DataStorage\Database\Pool;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class TemplateMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new TemplateMapper($this->dbPool->get());

        $template = new Template();

        $template->setCreatedBy(1);
        $template->setCreatedAt($date = new \DateTime('2000-05-05'));
        $template->setName('Title');
        $template->setStatus(ReporterStatus::ACTIVE);
        $template->setDescription('Description');
        $template->setDatatype(TemplateDataType::OTHER);
        $template->setExpected(['source1.csv', 'source2.csv']);
        $template->setSource(9);

        $id = $mapper->create($template);
        $this->assertGreaterThan(0, $template->getId());
        $this->assertEquals($id, $template->getId());

        $templateR = $mapper->get($template->getId());
        $this->assertEquals($date->format('Y-m-d'), $templateR->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals($template->getCreatedBy(), $templateR->getCreatedBy());
        $this->assertEquals($template->getDescription(), $templateR->getDescription());
        $this->assertEquals($template->getName(), $templateR->getName());
        $this->assertEquals($template->getStatus(), $templateR->getStatus());
        $this->assertEquals($template->getDatatype(), $templateR->getDatatype());
        $this->assertEquals($template->getExpected(), $templateR->getExpected());
    }

    public function testNewest()
    {
        $mapper = new TemplateMapper($this->dbPool->get());
        $newest = $mapper->getNewest(1);

        $this->assertEquals(1, count($newest));
    }
}

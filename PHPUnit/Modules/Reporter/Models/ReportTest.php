<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Reporter\Models;

use Modules\Reporter\Models\Report;
use Modules\Reporter\Models\ReporterStatus;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class ReportTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $report = new Report();

        $this->assertEquals(0, $report->getId());
        $this->assertEquals(0, $report->getCreatedBy());
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $report->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals('', $report->getTitle());
        $this->assertEquals(ReporterStatus::INACTIVE, $report->getStatus());
        $this->assertEquals('', $report->getDescription());
        $this->assertEquals(0, $report->getTemplate());
        $this->assertEquals(0, $report->getSource());
    }

    public function testSetGet()
    {
        $report = new Report();

        $report->setCreatedBy(1);
        $this->assertEquals(1, $report->getCreatedBy());

        $report->setCreatedAt($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $report->getCreatedAt()->format('Y-m-d'));

        $report->setTitle('Title');
        $this->assertEquals('Title', $report->getTitle());

        $report->setStatus(ReporterStatus::ACTIVE);
        $this->assertEquals(ReporterStatus::ACTIVE, $report->getStatus());

        $report->setDescription('Description');
        $this->assertEquals('Description', $report->getDescription());

        $report->setTemplate(11);
        $this->assertEquals(11, $report->getTemplate());

        $report->setSource(4);
        $this->assertEquals(4, $report->getSource());
    }
}

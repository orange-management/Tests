<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Reporter;

use Modules\Media\Models\UploadStatus;
use Modules\Reporter\Models\TemplateDataType;
use phpOMS\ApplicationAbstract;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Message\Http\Request;
use phpOMS\Message\Http\Response;
use phpOMS\Module\ModuleFactory;
use phpOMS\Router\Router;
use phpOMS\Uri\Http;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../config.php';

class ControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $app    = null;
    protected $module = null;

    protected function setUp()
    {
        $this->app = new class extends ApplicationAbstract
        {
        };

        $this->app->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->app->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);

        $this->app->router = new Router();

        $this->module = ModuleFactory::getInstance('Reporter', $this->app);
    }

    /**
     * @group admin
     */
    public function testCreateTemplateEventCourse()
    {
        $media = ModuleFactory::getInstance('Media', $this->app);;

        $status = [
            [
                'status' => UploadStatus::OK,
                'extension' => 'php',
                'filename' => 'EventCourse.lang.php',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
            [
                'status' => UploadStatus::OK,
                'extension' => 'php',
                'filename' => 'EventCourse.pdf.php',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
            [
                'status' => UploadStatus::OK,
                'extension' => 'php',
                'filename' => 'EventCourse.tpl.php',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
            [
                'status' => UploadStatus::OK,
                'extension' => 'php',
                'filename' => 'EventCourse.xlsx.php',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
            [
                'status' => UploadStatus::OK,
                'extension' => 'php',
                'filename' => 'Worker.php',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
        ];

        $ids = $media->createDbEntries($status, 1);
        $request = new Request(new Http(''));
        $request->setData('name', 'Test template');
        $request->setData('description', 'Template description');
        $request->setData('datatype', TemplateDataType::OTHER);
        $request->setData('files', json_encode($ids));
        $request->setAccount(1);

        $response = new Response();
        $this->module->apiCreateTemplate($request, $response);

        $this->assertEquals(1, $response->get(''));
    }

    /**
     * @group admin
     */
    public function testCreateReportEventCourse()
    {
        $media = ModuleFactory::getInstance('Media', $this->app);;

        $status = [
            [
                'status' => UploadStatus::OK,
                'extension' => 'csv',
                'filename' => 'accounts.csv',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
            [
                'status' => UploadStatus::OK,
                'extension' => 'csv',
                'filename' => 'costcenters.csv',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
            [
                'status' => UploadStatus::OK,
                'extension' => 'csv',
                'filename' => 'costobjects.csv',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
            [
                'status' => UploadStatus::OK,
                'extension' => 'csv',
                'filename' => 'crm.csv',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
            [
                'status' => UploadStatus::OK,
                'extension' => 'csv',
                'filename' => 'entries.csv',
                'path' => 'Demo/Modules/Reporter/EventCourse',
                'size' => 1,
            ],
        ];

        $ids = $media->createDbEntries($status, 1);

        $request = new Request(new Http(''));
        $request->setData('name', 'Test report');
        $request->setData('template', 1);
        $request->setData('files', json_encode($ids));
        $request->setAccount(1);

        $response = new Response();
        $this->module->apiCreateReport($request, $response);

        $this->assertGreaterThan(0, $response->get(''));
    }

}

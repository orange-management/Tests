<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Tasks\Models;

use Modules\Tasks\Models\Task;
use Modules\Tasks\Models\TaskElement;
use Modules\Tasks\Models\TaskStatus;
use Modules\Tasks\Models\TaskType;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class TaskTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $task = new Task();

        $this->assertEquals(0, $task->getId());
        $this->assertEquals(0, $task->getCreatedBy());
        $this->assertEquals('', $task->getTitle());
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $task->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals(new \DateTime('now'), $task->getDone());
        $this->assertEquals((new \DateTime('now'))->modify('+1 day'), $task->getDue());
        $this->assertEquals(TaskStatus::OPEN, $task->getStatus());
        $this->assertEquals(TaskType::SINGLE, $task->getType());
        $this->assertEquals([], $task->getTaskElements());
        $this->assertEquals('', $task->getDescription());
        $this->assertInstanceOf('\Modules\Tasks\Models\NullTaskElement', $task->getTaskElement(1));
    }

    public function testSetGet()
    {
        $task = new Task();

        $task->setCreatedBy(1);
        $this->assertEquals(1, $task->getCreatedBy());

        $task->setCreatedAt($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $task->getCreatedAt()->format('Y-m-d'));

        $task->setTitle('Title');
        $this->assertEquals('Title', $task->getTitle());

        $task->setDone($date = new \DateTime('2000-05-06'));
        $this->assertEquals($date->format('Y-m-d'), $task->getDone()->format('Y-m-d'));

        $task->setDue($date = new \DateTime('2000-05-07'));
        $this->assertEquals($date->format('Y-m-d'), $task->getDue()->format('Y-m-d'));

        $task->setStatus(TaskStatus::DONE);
        $this->assertEquals(TaskStatus::DONE, $task->getStatus());

        $id = [];
        $id[] = $task->addElement(new TaskElement());
        $id[] = $task->addElement(new TaskElement());
        $success = $task->removeElement(99);

        $this->assertFalse($success);

        $success = $task->removeElement($id[1]);
        $this->assertTrue($success);

        $this->assertEquals(0, $task->getTaskElements()[0]->getId());
        $this->assertEquals(0, $task->getTaskElement(0)->getId());

        $task->setDescription('Description');
        $this->assertEquals('Description', $task->getDescription());

        $this->assertInstanceOf('\Modules\Tasks\Models\TaskElement', $task->getTaskElement(1));
    }
}

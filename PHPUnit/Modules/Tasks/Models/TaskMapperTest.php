<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Tasks\Models;

use Modules\Tasks\Models\Task;
use Modules\Tasks\Models\TaskElement;
use Modules\Tasks\Models\TaskMapper;
use Modules\Tasks\Models\TaskStatus;
use Modules\Tasks\Models\TaskType;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Utils\RnG\DateTime;
use phpOMS\Utils\RnG\Text;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class TaskMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new TaskMapper($this->dbPool->get());

        $task = new Task();

        $task->setCreatedBy(1);
        $task->getSchedule()->setCreatedBy(1);
        $task->setCreatedAt(new \DateTime('2000-05-05'));
        $task->setTitle('Title');
        $task->setStatus(TaskStatus::DONE);
        $task->setDescription('Description');
        $task->setDone(new \DateTime('2000-05-06'));
        $task->setDue(new \DateTime('2000-05-05'));

        $taskElement1 = new TaskElement();
        $taskElement1->setDescription('Desc1');
        $taskElement1->setCreatedBy(1);
        $task->addElement($taskElement1);

        $taskElement2 = new TaskElement();
        $taskElement2->setDescription('Desc2');
        $taskElement2->setCreatedBy(1);
        $task->addElement($taskElement2);

        $id = $mapper->create($task);
        $this->assertGreaterThan(0, $task->getId());
        $this->assertEquals($id, $task->getId());

        $taskR = $mapper->get($task->getId());
        $this->assertEquals($task->getCreatedAt()->format('Y-m-d'), $taskR->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals($task->getCreatedBy(), $taskR->getCreatedBy());
        $this->assertEquals($task->getDescription(), $taskR->getDescription());
        $this->assertEquals($task->getTitle(), $taskR->getTitle());
        $this->assertEquals($task->getStatus(), $taskR->getStatus());
        $this->assertEquals($task->getType(), $taskR->getType());
        $this->assertEquals($task->getDone()->format('Y-m-d'), $taskR->getDone()->format('Y-m-d'));
        $this->assertEquals($task->getDue()->format('Y-m-d'), $taskR->getDue()->format('Y-m-d'));

        $expected = $task->getTaskElements();
        $actual   = $taskR->getTaskElements();
        $this->assertEquals(end($expected)->getDescription(), end($actual)->getDescription());
    }

    public function testNewest()
    {
        $mapper = new TaskMapper($this->dbPool->get());
        $newest = $mapper->getNewest(1);

        $this->assertEquals(1, count($newest));
    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume()
    {

        // todo: foreach account
        $mapper = new TaskMapper($this->dbPool->get());

        $iEnd = mt_rand(0, 300);
        for ($i = 0; $i < $iEnd; $i++) {
            $lorem = new Text();
            $lorem->setParagraphs(false);
            $task = new Task();

            $task->setCreatedBy(1);
            $task->getSchedule()->setCreatedBy(1);
            $task->setCreatedAt(new \DateTime(date("Y-m-d H:i:s", mt_rand(1356998400, (new \DateTime('now'))->getTimestamp()))));
            $task->setTitle($lorem->generateText(mt_rand(3, 10)));
            $task->setStatus(TaskStatus::DONE);
            $lorem->setParagraphs(true);
            $task->setDescription($lorem->generateText(mt_rand(20, 150)));

            if (mt_rand(0, 10) < 6) {
                $task->setDone(new \DateTime(date("Y-m-d H:i:s", mt_rand($task->getCreatedAt()->getTimestamp(), (new \DateTime('now'))->getTimestamp()))));
                $task->setStatus(TaskStatus::DONE);
            } else {
                if (($status = mt_rand(0, 10)) < 2) {
                    $task->setStatus(TaskStatus::CANCELED);
                } elseif ($status < 4) {
                    $task->setStatus(TaskStatus::SUSPENDED);
                } elseif ($status < 7) {
                    $task->setStatus(TaskStatus::WORKING);
                } else {
                    $task->setStatus(TaskStatus::OPEN);
                }
            }

            $task->setDue(new \DateTime(date("Y-m-d H:i:s", mt_rand($task->getCreatedAt()->getTimestamp(), (new \DateTime('now'))->getTimestamp()))));

            $element = new TaskElement();
            $element->setForwarded($task->getCreatedBy());
            $element->setCreatedAt($task->getCreatedAt());
            $element->setCreatedBy($task->getCreatedBy());
            $element->setDue($task->getDue());
            $element->setStatus(TaskStatus::OPEN);

            $jEnd = mt_rand(0, 10);

            if ($jEnd == 0) {
                $element->setStatus($task->getStatus());
            }

            $task->addElement($element);
            $previous = $element;

            $lorem->setParagraphs(true);
            for ($j = 0; $j < $jEnd; $j++) {
                $element = new TaskElement();
                $element->setForwarded(1);
                $element->setCreatedAt(new \DateTime(date("Y-m-d H:i:s", mt_rand($previous->getCreatedAt()->getTimestamp(), $task->getDone()->getTimestamp()))));
                $element->setCreatedBy($previous->getForwarded() === 0 ? $previous->getCreatedBy() : $previous->getForwarded());
                $element->setDue($task->getDue());
                $element->setDescription($lorem->generateText(mt_rand(0, 150)));

                if (($status = mt_rand(0, 10)) < 1) {
                    $element->setStatus(TaskStatus::DONE);
                } elseif ($status < 2) {
                    $element->setStatus(TaskStatus::CANCELED);
                } elseif ($status < 4) {
                    $element->setStatus(TaskStatus::SUSPENDED);
                } elseif ($status < 7) {
                    $element->setStatus(TaskStatus::WORKING);
                } else {
                    $element->setStatus(TaskStatus::OPEN);
                }

                if ($j + 1 >= $jEnd) {
                    $element->setStatus($task->getStatus());
                }

                $task->addElement($element);
                $previous = $element;
            }

            $mapper->create($task);
        }
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Tasks\Models;

use Modules\Tasks\Models\TaskElement;
use Modules\Tasks\Models\TaskStatus;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class TaskElementTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $task = new TaskElement();

        $this->assertEquals(0, $task->getId());
        $this->assertEquals(0, $task->getCreatedBy());
        $this->assertEquals((new \DateTime('now'))->format('Y-m-d'), $task->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals((new \DateTime('now'))->modify('+1 day'), $task->getDue());
        $this->assertEquals(TaskStatus::OPEN, $task->getStatus());
        $this->assertEquals('', $task->getDescription());
        $this->assertEquals(0, $task->getForwarded());
        $this->assertEquals(0, $task->getTask());
    }

    public function testSetGet()
    {
        $task = new TaskElement();

        $task->setCreatedBy(1);
        $this->assertEquals(1, $task->getCreatedBy());

        $task->setCreatedAt($date = new \DateTime('2000-05-05'));
        $this->assertEquals($date->format('Y-m-d'), $task->getCreatedAt()->format('Y-m-d'));

        $task->setDue($date = new \DateTime('2000-05-07'));
        $this->assertEquals($date->format('Y-m-d'), $task->getDue()->format('Y-m-d'));

        $task->setStatus(TaskStatus::DONE);
        $this->assertEquals(TaskStatus::DONE, $task->getStatus());

        $task->setDescription('Description');
        $this->assertEquals('Description', $task->getDescription());

        $task->setTask(2);
        $this->assertEquals(2, $task->getTask());

        $task->setForwarded(3);
        $this->assertEquals(3, $task->getForwarded());
    }
}

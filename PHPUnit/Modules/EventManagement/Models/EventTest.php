<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\EventManagement\Models;

use Modules\EventManagement\Models\Event;
use Modules\EventManagement\Models\EventType;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class EventTest extends \PHPUnit_Framework_TestCase
{
    public function testDefult()
    {
        $event = new Event();

        $this->assertEquals(0, $event->getId());
        $this->assertEquals(EventType::DEFAULT, $event->getType());
        $this->assertInstanceOf('\Modules\Calendar\Models\Event', $event->getEvent());
        $this->assertEquals(0, $event->getCosts()->getInt());
        $this->assertEquals(0, $event->getBudget()->getInt());
        $this->assertEquals(0, $event->getEarnigns()->getInt());
        $this->assertFalse($event->removeTask(2));
        $this->assertEmpty($event->getTasks());
    }

    public function testSetGet()
    {
        $event = new Event();

        $event->setType(EventType::SEMINAR);
        $this->assertEquals(EventType::SEMINAR, $event->getType());

        $money = new Money();
        $money->setString('1.23');

        $event->setCosts($money);
        $this->assertEquals($money->getAmount(), $event->getCosts()->getAmount());

        $event->setBudget($money);
        $this->assertEquals($money->getAmount(), $event->getBudget()->getAmount());

        $event->setEarnings($money);
        $this->assertEquals($money->getAmount(), $event->getEarnings()->getAmount());

        $task = new Task();
        $task->setTitle('A');

        $event->addTask($task);
        $this->assertEquals('A', $event->getTask(0)->getTitle());

        $this->assertTrue($event->removeTask(0));
        $this->assertEquals(0, $event->countTasks());

        $event->addTask($task);
        $this->assertEquals(1, count($event->getTasks()));
    }
}

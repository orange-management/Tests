<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\EventManagement\Models;

use Modules\EventManagement\Models\Event;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Utils\RnG\DateTime;
use phpOMS\Utils\RnG\Text;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class EventMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper = new EventMapper($this->dbPool->get());

        $event = new Event();

        $event->setType(EventType::SEMINAR);
        $event->setName('Eventname');

        $money = new Money();
        $money->setString('1.23');

        $event->setCosts($money);
        $event->setBudget($money);
        $event->setEarnings($money);

        $task = new Task();
        $task->setTitle('A');

        $task2 = new Task();
        $task2->setTitle('B');

        $event->addTask($task);
        $event->addTask($task2);

        $id = $mapper->create($event);
        $this->assertGreaterThan(0, $event->getId());
        $this->assertEquals($id, $event->getId());

        $eventR = $mapper->get($event->getId());

        $this->assertEquals($event->getEvent()->getName(), $eventR->getEvent()->getName());
        $this->assertEquals($event->countTasks(), $eventR->countTasks());
        $this->assertEquals($event->getCosts()->getAmount(), $eventR->getCosts()->getAmount());
        $this->assertEquals($event->getBudget()->getAmount(), $eventR->getBudget()->getAmount());
        $this->assertEquals($event->getEarnings()->getAmount(), $eventR->getEarnings()->getAmount());
        
    }

    public function testNewest()
    {
        $mapper = new EventMapper($this->dbPool->get());
        $newest = $mapper->getNewest(1);

        $this->assertEquals(1, count($newest));
    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume()
    {

        // todo: foreach account
        $mapper = new EventMapper($this->dbPool->get());

        $iEnd = mt_rand(20, 300);
        for ($i = 0; $i < $iEnd; $i++) {
            $lorem = new Text();
            $lorem->setParagraphs(false);

            $event = new Event();
            $event->getEvent()->setCreatedBy(1);
            $event->getEvent()->setCreatedAt(new \DateTime(date("Y-m-d H:i:s", mt_rand(1356998400, (new \DateTime('now'))->getTimestamp()))));

            $event->setType(EventType::getRandom());
            $event->setName($lorem->generateText(mt_rand(3, 10)));

            $money = new Money();
            $money->setInt(mt_rand(PHP_INT_MIN, PHP_INT_MAX));
            $event->setCosts($money);

            $money = new Money();
            $money->setInt(mt_rand(PHP_INT_MIN, PHP_INT_MAX));
            $event->setBudget($money);

            $money = new Money();
            $money->setInt(mt_rand(PHP_INT_MIN, PHP_INT_MAX));
            $event->setEarnings($money);

            $iEnd2 = mt_rand(0, 50);
            for ($j = 0; $i < $iEnd2; $j++) {
                $task = new Task();
                $task->setTitle($lorem->generateText(mt_rand(3, 10)));

                $event->addTask($task);
            }

            $mapper->create($event);
        }
    }
}

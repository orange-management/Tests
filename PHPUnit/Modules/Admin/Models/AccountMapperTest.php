<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Modules\Tasks\Models;

use Modules\Admin\Models\Account;
use Modules\Admin\Models\AccountMapper;
use phpOMS\Account\AccountStatus;
use phpOMS\Account\AccountType;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Utils\RnG\Name;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class AccountMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testCRUD()
    {
        $mapper  = new AccountMapper($this->dbPool->get());
        $account = new Account();

        $account->setName1('Donald');
        $account->setName2('Fauntleroy');
        $account->setName3('Duck');
        $account->setEmail('d.duck@duckburg.com');
        $account->setStatus(AccountStatus::ACTIVE);
        $account->setType(AccountType::USER);

        $id = $mapper->create($account);
        $this->assertGreaterThan(0, $account->getId());
        $this->assertEquals($id, $account->getId());

        $accountR = $mapper->get($account->getId());
        $this->assertEquals($account->getCreatedAt()->format('Y-m-d'), $accountR->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals($account->getName1(), $accountR->getName1());
        $this->assertEquals($account->getName2(), $accountR->getName2());
        $this->assertEquals($account->getName3(), $accountR->getName3());
        $this->assertEquals($account->getStatus(), $accountR->getStatus());
        $this->assertEquals($account->getType(), $accountR->getType());
        $this->assertEquals($account->getEmail(), $accountR->getEmail());
    }

    /**
     * @group         volume
     * @slowThreshold 15000
     */
    public function testVolume()
    {
        // todo: foreach account
        $mapper = new AccountMapper($this->dbPool->get());

        $iEnd = mt_rand(100, 300);
        for ($i = 0; $i < $iEnd; $i++) {
            $account = new Account();
            $account->setName1(Name::generateName(['male', 'female'], 'western'));
            $account->setName2(Name::generateName(['male', 'female'], 'western'));
            $account->setName3(Name::generateName(['family'], 'western'));
            $account->setEmail($account->getName1()[0] . '.' . $account->getName2() . '@duckburg.com');
            $account->setCreatedAt(new \DateTime(date("Y-m-d H:i:s", mt_rand(1356998400, (new \DateTime('now'))->getTimestamp()))));

            if(($rand = mt_rand(1, 10)) < 5) {
                $account->setStatus(AccountStatus::ACTIVE);
            } elseif($rand < 8) {
                $account->setStatus(AccountStatus::INACTIVE);
            } elseif($rand < 9) {
                $account->setStatus(AccountStatus::BANNED);
            } else {
                $account->setStatus(AccountStatus::TIMEOUT);
            }

            if(mt_rand(1, 10) < 6) {
                $account->setType(AccountType::USER);
            } else {
                $account->setType(AccountType::GROUP);
            }

            $mapper->create($account);
        }
    }
}

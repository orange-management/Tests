<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Localization;

use phpOMS\Localization\ISO3166Enum;
use phpOMS\Localization\ISO4217Enum;
use phpOMS\Localization\ISO639x1Enum;
use phpOMS\Localization\Localization;
use phpOMS\Localization\TimeZoneEnumArray;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class LocalizationTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $localization = new Localization();
        $this->assertObjectHasAttribute('country', $localization);
        $this->assertObjectHasAttribute('timezone', $localization);
        $this->assertObjectHasAttribute('language', $localization);
        $this->assertObjectHasAttribute('currency', $localization);
        $this->assertObjectHasAttribute('decimal', $localization);
        $this->assertObjectHasAttribute('thousands', $localization);
        $this->assertObjectHasAttribute('datetime', $localization);
        $this->assertObjectHasAttribute('lang', $localization);
    }

    public function testDefault()
    {
        $localization = new Localization();
        $this->assertTrue(ISO3166Enum::isValidValue($localization->getCountry()));
        $this->assertTrue(TimeZoneEnumArray::isValidValue($localization->getTimezone()));
        $this->assertTrue(ISO639x1Enum::isValidValue($localization->getLanguage()));
        $this->assertTrue(ISO4217Enum::isValidValue($localization->getCurrency()));
        $this->assertEquals('.', $localization->getDecimal());
        $this->assertEquals(',', $localization->getThousands());
        $this->assertEquals('Y-m-d H:i:s', $localization->getDatetime());
        $this->assertEquals([], $localization->getLang());
    }

    /**
     * @expectedException \phpOMS\Datatypes\Exception\InvalidEnumValue
     */
    public function testInvalidLanguage()
    {
        $localization = new Localization();
        $localization->setLanguage('abc');
    }

    /**
     * @expectedException \phpOMS\Datatypes\Exception\InvalidEnumValue
     */
    public function testInvalidCountry()
    {
        $localization = new Localization();
        $localization->setLanguage('abc');
    }

    /**
     * @expectedException \phpOMS\Datatypes\Exception\InvalidEnumValue
     */
    public function testInvalidTimezone()
    {
        $localization = new Localization();
        $localization->setTimezone('abc');
    }

    /**
     * @expectedException \phpOMS\Datatypes\Exception\InvalidEnumValue
     */
    public function testInvalidCurrency()
    {
        $localization = new Localization();
        $localization->setCurrency('abc');
    }

    public function testGetSet()
    {
        $localization = new Localization();

        $localization->setCountry(ISO3166Enum::_DE);
        $this->assertEquals(ISO3166Enum::_DE, $localization->getCountry());

        $localization->setTimezone(TimeZoneEnumArray::get(315));
        $this->assertEquals(TimeZoneEnumArray::get(315), $localization->getTimezone());

        $localization->setLanguage(ISO639x1Enum::_DE);
        $this->assertEquals(ISO639x1Enum::_DE, $localization->getLanguage());

        $localization->setLang(['Test' => 'Test string']);
        $this->assertEquals(['Test' => 'Test string'], $localization->getLang());

        $localization->setCurrency(ISO4217Enum::C_EUR);
        $this->assertEquals(ISO4217Enum::C_EUR, $localization->getCurrency());

        $localization->setDatetime('Y-m-d H:i:s');
        $this->assertEquals('Y-m-d H:i:s', $localization->getDatetime());

        $localization->setDecimal(',');
        $this->assertEquals(',', $localization->getDecimal());

        $localization->setThousands('.');
        $this->assertEquals('.', $localization->getThousands());
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Localization;

use phpOMS\Localization\L11nManager;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class L11nManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $l11nManager = new L11nManager();
        $this->assertObjectHasAttribute('language', $l11nManager);
    }

    public function testDefault()
    {
        $l11nManager = new L11nManager();
        $this->assertFalse($l11nManager->isLanguageLoaded('en'));
        $this->assertEquals([], $l11nManager->getLanguage('en'));
        $this->assertEquals([], $l11nManager->getLanguage('en', 'Admin'));
    }

    /**
     * @expectedException \Exception
     */
    public function testInvalidCurrency()
    {
        $expected = [
            'en' => [
                'Admin' => [
                    'Test' => 'Test string'
                ]
            ]
        ];

        $localization = new L11nManager();
        $localization->loadLanguage('en', 'doesNotExist', $expected);
    }

    public function testGetSet()
    {
        $expected = [
            'en' => [
                'Admin' => [
                    'Test' => 'Test string'
                ]
            ]
        ];

        $expected2 = [
            'en' => [
                'Admin' => [
                    'Test2' => 'Test string2'
                ]
            ]
        ];

        $expected3 = [
            'en' => [
                'Admin' => [
                    'Test' => 'Test string',
                    'Test2' => 'Test string2'
                ]
            ]
        ];

        $l11nManager = new L11nManager();
        $l11nManager->loadLanguage('en', 'Admin', $expected['en']);
        $l11nManager->loadLanguage('en', 'Admin', $expected2['en']);
        $this->assertTrue($l11nManager->isLanguageLoaded('en'));
        $this->assertEquals($expected3['en'], $l11nManager->getLanguage('en'));
        $this->assertEquals($expected3['en']['Admin'], $l11nManager->getLanguage('en', 'Admin'));

    }
}

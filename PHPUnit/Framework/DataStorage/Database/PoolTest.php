<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\DataStorage\Database;

use phpOMS\DataStorage\Database\Connection\MysqlConnection;
use phpOMS\DataStorage\Database\DatabaseStatus;
use phpOMS\DataStorage\Database\Pool;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../config.php';

class PoolTest extends \PHPUnit_Framework_TestCase
{
    public function testBasicConnection()
    {
        $dbPool = new Pool();
        /** @var array $CONFIG */
        $dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);

        $this->assertEquals($dbPool->get()->getStatus(), DatabaseStatus::OK);
    }

    public function testGetSet()
    {
        $dbPool = new Pool();
        /** @var array $CONFIG */

        $this->assertTrue($dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]));
        $this->assertFalse($dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]));

        $this->assertInstanceOf('\phpOMS\DataStorage\Database\Connection\ConnectionAbstract', $dbPool->get());
        $this->assertFalse($dbPool->get('doesNotExist'));
        $this->assertEquals($dbPool->get('core'), $dbPool->get());

        $this->assertFalse($dbPool->remove('cores'));
        $this->assertTrue($dbPool->remove('core'));

        $this->assertFalse($dbPool->get());

        $this->assertTrue($dbPool->add('core', new MysqlConnection($GLOBALS['CONFIG']['db']['core']['masters'][0])));
        $this->assertFalse($dbPool->add('core', new MysqlConnection($GLOBALS['CONFIG']['db']['core']['masters'][0])));
    }
}

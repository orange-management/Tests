<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\DataStorage\Database\Query;

use phpOMS\DataStorage\Database\Connection\MysqlConnection;
use phpOMS\DataStorage\Database\Query\Builder;

require_once __DIR__ . '/../../../../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../../../../config.php';

class BuilderTest extends \PHPUnit_Framework_TestCase
{
    protected $con    = null;

    protected function setUp()
    {
        $this->con = new MysqlConnection($GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    public function testMysqlSelect()
    {
        $query = new Builder($this->con);
        $sql   = 'SELECT `a`.`test` FROM `a` WHERE `a`.`test` = 1;';
        $this->assertEquals($sql, $query->select('a.test')->from('a')->where('a.test', '=', 1)->toSql());

        $query = new Builder($this->con);
        $sql   = 'SELECT `a`.`test`, `b`.`test` FROM `a`, `b` WHERE `a`.`test` = \'abc\';';
        $this->assertEquals($sql, $query->select('a.test', 'b.test')->from('a', 'b')->where('a.test', '=', 'abc')->toSql());

        $query            = new Builder($this->con);
        $sql              = 'SELECT `a`.`test`, `b`.`test` FROM `a`, `b` WHERE `a`.`test` = \'abc\' AND `b`.`test` = 2;';
        $systemIdentifier = '`';
        $this->assertEquals($sql, $query->select('a.test', function () {
            return '`b`.`test`';
        })->from('a', function () use ($systemIdentifier) {
            return $systemIdentifier . 'b' . $systemIdentifier;
        })->where(['a.test', 'b.test'], ['=', '='], ['abc', 2], ['and', 'and'])->toSql());

        $query = new Builder($this->con);
        $sql   = 'SELECT `a`.`test`, `b`.`test` FROM `a`, `b` WHERE `a`.`test` = \'abc\' ORDER BY `a`.`test` ASC, `b`.`test` DESC;';
        $this->assertEquals($sql, $query->select('a.test', 'b.test')->from('a', 'b')->where('a.test', '=', 'abc')->orderBy(['a.test',
                                                                                                                            'b.test', ], ['ASC',
                                                                                                                                        'DESC', ])->toSql());
    }

    public function testMysqlInsert()
    {
        $query = new Builder($this->con);
        $sql   = 'INSERT INTO `a` VALUES (1, \'test\');';
        $this->assertEquals($sql, $query->insert()->into('a')->values(1, 'test')->toSql());

        $query = new Builder($this->con);
        $sql   = 'INSERT INTO `a` (`test`, `test2`) VALUES (1, \'test\');';
        $this->assertEquals($sql, $query->insert('test', 'test2')->into('a')->values(1, 'test')->toSql());
    }
}

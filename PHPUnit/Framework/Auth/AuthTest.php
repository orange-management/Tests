<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Account;

use phpOMS\Auth\Auth;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\DataStorage\Session\ConsoleSession;
use phpOMS\DataStorage\Session\HttpSession;
use phpOMS\DataStorage\Session\SocketSession;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class AuthTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $dbPool = new Pool();

        /** @var array $CONFIG */
        $dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);

        $auth = new Auth($dbPool->get(), $GLOBALS['httpSession']);
        $this->assertInstanceOf('\phpOMS\Auth\Auth', $auth);

        /* Testing members */
        $this->assertObjectHasAttribute('session', $auth);
        $this->assertObjectHasAttribute('connection', $auth);
    }

    public function testWithHttpSession()
    {
        $dbPool = new Pool();

        /** @var array $CONFIG */
        $dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);

        $auth = new Auth($dbPool->get(), $GLOBALS['httpSession']);

        $this->assertFalse($auth->authenticate());

        $auth->login('admin', 'orange');
        $this->assertNotEquals(false, $auth->authenticate());
        $auth->logout();
        $this->assertFalse($auth->authenticate());
    }

    public function testWithSocketSession()
    {
        $dbPool = new Pool();

        /** @var array $CONFIG */
        $dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);

        $session = new SocketSession();

        $auth = new Auth($dbPool->get(), $session);

        $this->assertFalse($auth->authenticate());
    }

    public function testWithConsoleSession()
    {
        $dbPool = new Pool();

        /** @var array $CONFIG */
        $dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);

        $session = new ConsoleSession();

        $auth = new Auth($dbPool->get(), $session);

        $this->assertFalse($auth->authenticate());
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Account;

use phpOMS\Asset\AssetManager;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class AssetManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $manager = new AssetManager();
        $this->assertInstanceOf('\phpOMS\Asset\AssetManager', $manager);

        /* Testing members */
        $this->assertObjectHasAttribute('assets', $manager);
    }

    public function testDefault()
    {
        $manager = new AssetManager();

        /* Testing default values */
        $this->assertNull($manager->get('myAsset'));
        $this->assertEquals(0, $manager->count());
    }

    public function testSetGet()
    {
        $manager = new AssetManager();

        /* Test set/get/count */
        $manager->set('first', 'FirstUri');
        $set = $manager->set('myAsset', 'AssetUri');
        $this->assertTrue($set);
        $this->assertEquals('AssetUri', $manager->get('myAsset'));
        $this->assertEquals(2, $manager->count());

        $set = $manager->set('myAsset', 'AssetUri2', false);
        $this->assertFalse($set);
        $this->assertEquals('AssetUri', $manager->get('myAsset'));
        $this->assertEquals(2, $manager->count());

        $set = $manager->set('myAsset', 'AssetUri2');
        $this->assertTrue($set);
        $this->assertEquals('AssetUri2', $manager->get('myAsset'));
        $this->assertEquals(2, $manager->count());

        $set = $manager->set('myAsset', 'AssetUri3', true);
        $this->assertTrue($set);
        $this->assertEquals('AssetUri3', $manager->get('myAsset'));
        $this->assertEquals(2, $manager->count());

        /* Test remove */
        $rem = $manager->remove('myAsset');
        $this->assertTrue($rem);
        $this->assertEquals(1, $manager->count());

        $this->assertNull($manager->get('myAsset'));

        $rem = $manager->remove('myAsset');
        $this->assertFalse($rem);
        $this->assertEquals(1, $manager->count());

    }
}

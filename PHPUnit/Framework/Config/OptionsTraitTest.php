<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Config;

use phpOMS\Config\OptionsTrait;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class OptionsTraitTest extends \PHPUnit_Framework_TestCase
{

    public function testOptionTrait()
    {
        $class = new class {
            use OptionsTrait;
        };

        /* Testing members */
        $this->assertObjectHasAttribute('options', $class);
    }

    public function testDefault()
    {
        $class = new class {
            use OptionsTrait;
        };

        $this->assertFalse($class->exists('someKey'));
        $this->assertNull($class->getOption('someKey'));
    }

    public function testSetGet()
    {
        $class = new class {
            use OptionsTrait;
        };

        $this->assertTrue($class->setOption('a', 'value1'));
        $this->assertTrue($class->exists('a'));
        $this->assertEquals('value1', $class->getOption('a'));

        $this->assertTrue($class->setOption('a', 'value2'));
        $this->assertTrue($class->exists('a'));
        $this->assertEquals('value2', $class->getOption('a'));

        $this->assertTrue($class->setOption('a', 'value3', true));
        $this->assertTrue($class->exists('a'));
        $this->assertEquals('value3', $class->getOption('a'));

        $this->assertFalse($class->setOption('a', 'value4', false));
        $this->assertTrue($class->exists('a'));
        $this->assertEquals('value3', $class->getOption('a'));

        $this->assertTrue($class->setOptions(['b' => 2, 'c' => '3'], true));
        $this->assertFalse($class->setOptions(['b' => 4, 'c' => '5'], false));
        $this->assertTrue($class->exists('a'));
        $this->assertTrue($class->exists('b'));
        $this->assertTrue($class->exists('c'));
        $this->assertEquals('value3', $class->getOption('a'));
        $this->assertEquals(2, $class->getOption('b'));
        $this->assertEquals(3, $class->getOption('c'));
    }
}

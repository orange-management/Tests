<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Uri;

use phpOMS\Uri\UriFactory;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class UriFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testDefault()
    {
        $this->assertFalse(UriFactory::getQuery('Invalid'));
    }

    public function testSetGet()
    {
        $this->assertTrue(UriFactory::setQuery('Valid', 'query1'));
        $this->assertEquals('query1', UriFactory::getQuery('Valid'));

        $this->assertTrue(UriFactory::setQuery('Valid', 'query2', true));
        $this->assertEquals('query2', UriFactory::getQuery('Valid'));

        $this->assertFalse(UriFactory::setQuery('Valid', 'query3', false));
        $this->assertEquals('query2', UriFactory::getQuery('Valid'));

        $this->assertTrue(UriFactory::setQuery('/valid2', 'query4'));
        $this->assertEquals('query4', UriFactory::getQuery('/valid2'));
    }

    public function testBuilder()
    {
        $uri = 'www.test-uri.com?id={@ID}&test={.mTest}&two={/path}&hash={#hash}&none=#none&found={/not}&v={/valid2}';

        $vars = [
            '@ID'    => 1,
            '.mTest' => 'someString',
            '/path'  => 'PATH',
            '#hash'  => 'test',
        ];

        $expected = 'www.test-uri.com?id=1&test=someString&two=PATH&hash=test&none=#none&found=/not&v=query4';

        $this->assertEquals($expected, UriFactory::build($uri, $vars));
    }
}

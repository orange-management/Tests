<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Dispatcher;


use phpOMS\ApplicationAbstract;
use phpOMS\Dispatcher\Dispatcher;
use phpOMS\Message\Http\Request;
use phpOMS\Message\Http\Response;
use phpOMS\Router\Router;
use phpOMS\Uri\Http;
use phpOMS\Views\ViewLayout;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class DispatcherTest extends \PHPUnit_Framework_TestCase
{
    protected $app = null;

    protected function setUp()
    {
        $this->app = new class extends ApplicationAbstract {};
        $this->app->router = new Router();
        $this->app->dispatcher = new Dispatcher($this->app);
    }

    public function testAttributes()
    {
        $this->assertObjectHasAttribute('controllers', $this->app->dispatcher);
    }

    /**
     * @expectedException \phpOMS\System\PathException
     */
    public function testInvalidControllerException()
    {
        $this->app->dispatcher->get('doesNotExist');
    }

    public function testGetSet()
    {
        $this->assertInstanceOf('\Modules\Admin\Controller', $this->app->dispatcher->get('\Modules\Admin\Controller'));
        $this->assertTrue(is_array($this->app->dispatcher->dispatch(function() { return true; }, new Request(new Http('')), new Response())));
        $this->assertTrue($this->app->dispatcher->dispatch(function($app, $request, $response, $data) { return true; }, new Request(new Http('')), new Response())[ViewLayout::UNDEFINED][0]);
    }
}

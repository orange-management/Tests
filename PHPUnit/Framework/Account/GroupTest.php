<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Account;

use phpOMS\Account\Group;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class GroupTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes() {
        $group = new Group();
        $this->assertInstanceOf('\phpOMS\Account\Group', $group);

        /* Testing members */
        $this->assertObjectHasAttribute('id', $group);
        $this->assertObjectHasAttribute('name', $group);
        $this->assertObjectHasAttribute('description', $group);
        $this->assertObjectHasAttribute('members', $group);
        $this->assertObjectHasAttribute('parents', $group);
        $this->assertObjectHasAttribute('permissions', $group);
    }

    public function testDefault() {
        $group = new Group();

        /* Testing default values */
        $this->assertTrue(is_int($group->getId()));
        $this->assertEquals(0, $group->getId());

        $this->assertTrue(is_string($group->getName()));
        $this->assertEquals('', $group->getName());

        $this->assertTrue(is_string($group->getDescription()));
        $this->assertEquals('', $group->getDescription());
    }

    public function testSetGet()
    {
        $group = new Group();

        $group->setName('Duck');
        $this->assertEquals('Duck', $group->getName());

        $group->setDescription('Animal');
        $this->assertEquals('Animal', $group->getDescription());
    }
}

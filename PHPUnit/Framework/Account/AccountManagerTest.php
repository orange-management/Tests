<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Account;

use phpOMS\Account\Account;
use phpOMS\Account\AccountManager;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class AccountManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes() {
        $manager = new AccountManager();
        $this->assertInstanceOf('\phpOMS\Account\AccountManager', $manager);

        /* Testing members */
        $this->assertObjectHasAttribute('accounts', $manager);
    }

    public function testDefault() {
        $manager = new AccountManager();

        /* Testing default values */
        $this->assertInstanceOf('\phpOMS\Account\NullAccount', $manager->get(0));
    }

    public function testSetGet()
    {
        $manager = new AccountManager();
        $account = new Account();

        $id = $manager->set($account);

        $this->assertEquals(0, $id);
        $this->assertEquals($account, $manager->get($id));

        $id = $manager->set($account);
        $this->assertNull($id);
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Account;

use phpOMS\Account\Account;
use phpOMS\Account\AccountStatus;
use phpOMS\Account\AccountType;
use phpOMS\Localization\Localization;
use phpOMS\Localization\NullLocalization;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class AccountTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes() {
        $account = new Account();
        $this->assertInstanceOf('\phpOMS\Account\Account', $account);

        /* Testing members */
        $this->assertObjectHasAttribute('id', $account);
        $this->assertObjectHasAttribute('name1', $account);
        $this->assertObjectHasAttribute('name2', $account);
        $this->assertObjectHasAttribute('name3', $account);
        $this->assertObjectHasAttribute('email', $account);
        $this->assertObjectHasAttribute('origin', $account);
        $this->assertObjectHasAttribute('login', $account);
        $this->assertObjectHasAttribute('lastActive', $account);
        $this->assertObjectHasAttribute('createdAt', $account);
        $this->assertObjectHasAttribute('permissions', $account);
        $this->assertObjectHasAttribute('groups', $account);
        $this->assertObjectHasAttribute('type', $account);
        $this->assertObjectHasAttribute('status', $account);
        $this->assertObjectHasAttribute('l11n', $account);
    }

    public function testDefault() {
        $account = new Account();

        /* Testing default values */
        $this->assertTrue(is_int($account->getId()));
        $this->assertEquals(0, $account->getId());

        $this->assertInstanceOf('\phpOMS\Localization\NullLocalization', $account->getL11n());

        $this->assertTrue(is_string($account->getName1()));
        $this->assertEquals('', $account->getName1());

        $this->assertTrue(is_string($account->getName2()));
        $this->assertEquals('', $account->getName2());

        $this->assertTrue(is_string($account->getName3()));
        $this->assertEquals('', $account->getName3());

        $this->assertTrue(is_string($account->getEmail()));
        $this->assertEquals('', $account->getEmail());

        $this->assertTrue(is_int($account->getStatus()));
        $this->assertEquals(AccountStatus::INACTIVE, $account->getStatus());

        $this->assertTrue(is_int($account->getType()));
        $this->assertEquals(AccountType::USER, $account->getType());

        $this->assertInstanceOf('\DateTime', $account->getLastActive());
        $this->assertInstanceOf('\DateTime', $account->getCreatedAt());
    }

    public function testSetGet()
    {
        $account = new Account();

        $account->setName1('Donald');
        $this->assertEquals('Donald', $account->getName1());

        $account->setName2('Fauntleroy');
        $this->assertEquals('Fauntleroy', $account->getName2());

        $account->setName3('Duck');
        $this->assertEquals('Duck', $account->getName3());

        $account->setEmail('d.duck@duckburg.com');
        $this->assertEquals('d.duck@duckburg.com', $account->getEmail());

        $account->setStatus(AccountStatus::ACTIVE);
        $this->assertEquals(AccountStatus::ACTIVE, $account->getStatus());

        $account->setType(AccountType::GROUP);
        $this->assertEquals(AccountType::GROUP, $account->getType());

        $account->setL11n(new NullLocalization());
        $this->assertInstanceOf('\phpOMS\Localization\NullLocalization', $account->getL11n());
        $account->setL11n(new Localization());
        $this->assertInstanceOf('\phpOMS\Localization\Localization', $account->getL11n());
        $this->assertNotInstanceOf('\phpOMS\Localization\NullLocalization', $account->getL11n());

        $datetime = new \DateTime('now');
        $account->updateLastActive();
        $this->assertEquals($datetime->format('Y-m-d h:i:s'), $account->getLastActive()->format('Y-m-d h:i:s'));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testEmailException() {
        $account = new Account();
        $account->setEmail('d.duck!@#%@duckburg');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testStatusException() {
        $account = new Account();
        $account->setStatus(99);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testTypeException() {
        $account = new Account();
        $account->setType(99);
    }
}

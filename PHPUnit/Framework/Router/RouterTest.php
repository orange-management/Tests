<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Router;

use phpOMS\Message\RequestMethod;
use phpOMS\Router\Router;
use phpOMS\Views\ViewLayout;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class RouterTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $router = new Router();
        $this->assertInstanceOf('\phpOMS\Router\Router', $router);

        $this->assertObjectHasAttribute('routes', $router);
    }

    public function testDefault()
    {
        $router = new Router();

        $this->assertEquals([], $router->route('/doesNotExist/yet', RequestMethod::GET));
    }

    public function testGetSet()
    {
        $router = new Router();

        $router->add('/some/path/.*\?id=\d*$', function() { return 'asdf'; }, RequestMethod::GET, ViewLayout::CLOSURE);
        $router->add('.*/some/other/path\?id=\d*$', '/path/to/Controller::functionName', RequestMethod::POST, ViewLayout::MAIN);

        $this->assertEquals([
            '/some/path/.*\?id=\d*$' => [
                ['dest' => function() { return 'asdf'; }, 'type' => ViewLayout::CLOSURE]
            ]
        ], $router->route('/some/path/variable?id=123', RequestMethod::GET));

        $this->assertEquals([
            '.*/some/other/path\?id=\d*$' => [
                ['dest' => '/path/to/Controller::functionName', 'type' => ViewLayout::MAIN]
            ]
        ], $router->route('here/some/other/path?id=123', RequestMethod::POST));
    }
}

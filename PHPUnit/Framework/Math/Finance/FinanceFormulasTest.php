<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Math\Finance;

use phpOMS\Math\Finance\FinanceFormulas;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';

class FinanceFormulasTest extends \PHPUnit_Framework_TestCase
{
    public function testAnnualPercentageYield()
    {
        $expected = 0.06168;

        $r   = 0.06;
        $n   = 12;
        $apy = FinanceFormulas::getAnnualPercentageYield($r, $n);

        $this->assertEquals(round($expected, 5), round($apy, 5));
        $this->assertEquals(round($r, 2), FinanceFormulas::getStateAnnualInterestRateOfAPY($apy, $n));
    }

    public function testFutureValueOfAnnuity()
    {
        $expected = 5204.04;

        $P   = 1000.00;
        $r   = 0.02;
        $n   = 5;
        $fva = FinanceFormulas::getFutureValueOfAnnuity($P, $r, $n);

        $this->assertEquals(round($expected, 2), round($fva, 2));
        $this->assertEquals($n, FinanceFormulas::getNumberOfPeriodsOfFVA($fva, $P, $r));
        $this->assertEquals(round($P, 2), round(FinanceFormulas::getPeriodicPaymentOfFVA($fva, $r, $n), 2));
    }

    public function testFutureValueOfAnnuityContinuousCompounding()
    {
        $expected = 12336.42;

        $cf    = 1000.00;
        $r     = 0.005;
        $t     = 12;
        $fvacc = FinanceFormulas::getFutureValueOfAnnuityConinuousCompounding($cf, $r, $t);

        $this->assertEquals(round($expected, 2), round($fvacc, 2));
        $this->assertEquals(round($cf, 2), round(FinanceFormulas::getCashFlowOfFVACC($fvacc, $r, $t), 2));
        $this->assertEquals($t, FinanceFormulas::getTimeOfFVACC($fvacc, $cf, $r));
    }

    public function testAnnuityPaymentPV()
    {
        $expected = 212.16;

        $pv = 1000.00;
        $r  = 0.02;
        $n  = 5;
        $p  = FinanceFormulas::getAnnuityPaymentPV($pv, $r, $n);

        $this->assertEquals(round($expected, 2), round($p, 2));
        $this->assertEquals($n, FinanceFormulas::getNumberOfAPPV($p, $pv, $r));
        $this->assertEquals(round($pv, 2), round(FinanceFormulas::getPresentValueOfAPPV($p, $r, $n), 2));
    }

    public function testAnnuityPaymentFV()
    {
        $expected = 192.16;

        $fv = 1000.00;
        $r  = 0.02;
        $n  = 5;
        $p  = FinanceFormulas::getAnnuityPaymentFV($fv, $r, $n);

        $this->assertEquals(round($expected, 2), round($p, 2));
        $this->assertEquals($n, FinanceFormulas::getNumberOfAPFV($p, $fv, $r));
        $this->assertEquals(round($fv, 2), round(FinanceFormulas::getFutureValueOfAPFV($p, $r, $n), 2));
    }

    public function testAnnutiyPaymentFactorPV()
    {
        $expected = 0.21216;

        $r = 0.02;
        $n = 5;
        $p = FinanceFormulas::getAnnutiyPaymentFactorPV($r, $n);

        $this->assertEquals(round($expected, 5), round($p, 5));
        $this->assertEquals($n, FinanceFormulas::getNumberOfAPFPV($p, $r));
    }

    public function testPresentValueOfAnnuity()
    {
        $expected = 4713.46;

        $P   = 1000.00;
        $r   = 0.02;
        $n   = 5;
        $pva = FinanceFormulas::getPresentValueOfAnnuity($P, $r, $n);

        $this->assertEquals(round($expected, 2), round($pva, 2));
        $this->assertEquals($n, FinanceFormulas::getNumberOfPeriodsOfPVA($pva, $P, $r));
        $this->assertEquals(round($P, 2), round(FinanceFormulas::getPeriodicPaymentOfPVA($pva, $r, $n), 2));
    }

    public function testPresentValueAnnuityFactor()
    {
        $expected = 4.7135;

        $r = 0.02;
        $n = 5;
        $p = FinanceFormulas::getPresentValueAnnuityFactor($r, $n);

        $this->assertEquals(round($expected, 4), round($p, 4));
        $this->assertEquals($n, FinanceFormulas::getPeriodsOfPVAF($p, $r));
    }

    public function testPresentValueOfAnnuityDue()
    {
        $expected = 454.60;

        $P = 100.00;
        $r = 0.05;
        $n = 5;

        $PV = FinanceFormulas::getPresentValueOfAnnuityDue($P, $r, $n);

        $this->assertEquals(round($expected, 2), round($PV, 2));
        $this->assertEquals(round($P, 2), FinanceFormulas::getPeriodicPaymentOfPVAD($PV, $r, $n));
        //$this->assertEquals($n, FinanceFormulas::getPeriodsOfPVAD($PV, $P, $r));
    }

    public function testFutureValueOfAnnuityDue()
    {
        $expected = 580.19;

        $P = 100.00;
        $r = 0.05;
        $n = 5;

        $FV = FinanceFormulas::getFutureValueOfAnnuityDue($P, $r, $n);

        $this->assertEquals(round($expected, 2), round($FV, 2));
        $this->assertEquals(round($P, 2), FinanceFormulas::getPeriodicPaymentOfFVAD($FV, $r, $n));
        $this->assertEquals($n, FinanceFormulas::getPeriodsOfFVAD($FV, $P, $r));
    }
}

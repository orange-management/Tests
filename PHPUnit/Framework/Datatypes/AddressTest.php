<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Datatypes;

use phpOMS\Datatypes\Address;
use phpOMS\Datatypes\Location;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class AddressTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $address = new Address();
        $this->assertObjectHasAttribute('recipient', $address);
        $this->assertObjectHasAttribute('fao', $address);
        $this->assertObjectHasAttribute('location', $address);
    }

    public function testDefault()
    {
        $expected = [
            'recipient' => '',
            'fao' => '',
            'location' => [
                'postal' => '',
                'city' => '',
                'country' => '',
                'address' => '',
                'state' => '',
                'geo' => [
                    'lat' => 0,
                    'long' => 0
                ],
            ]
        ];

        $address = new Address();
        $this->assertEquals('', $address->getRecipient());
        $this->assertEquals('', $address->getFAO());
        $this->assertInstanceOf('\phpOMS\Datatypes\Location', $address->getLocation());
        $this->assertEquals($expected, $address->toArray());
        $this->assertEquals(json_encode($expected), $address->toJson());
    }

    public function testGetSet()
    {
        $expected = [
            'recipient' => 'recipient',
            'fao' => 'fao',
            'location' => [
                'postal' => '',
                'city' => '',
                'country' => '',
                'address' => '',
                'state' => '',
                'geo' => [
                    'lat' => 0,
                    'long' => 0
                ],
            ]
        ];

        $address = new Address();
        $address->setFAO('fao');
        $address->setRecipient('recipient');
        $address->setLocation(new Location());

        $this->assertEquals('recipient', $address->getRecipient());
        $this->assertEquals('fao', $address->getFAO());
        $this->assertInstanceOf('\phpOMS\Datatypes\Location', $address->getLocation());
        $this->assertEquals($expected, $address->toArray());
        $this->assertEquals(json_encode($expected), $address->toJson());
    }
}

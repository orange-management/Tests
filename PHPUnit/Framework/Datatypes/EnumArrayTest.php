<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Datatypes;

use phpOMS\Datatypes\EnumArray;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class EnumArrayDemo extends EnumArray
{
    protected static $constants = [
        'ENUM1' => 1,
        'ENUM2' => 'abc',
    ];
}

;

class EnumArrayTest extends \PHPUnit_Framework_TestCase
{
    public function testGetSet()
    {
        $this->assertEquals(1, EnumArrayDemo::get('ENUM1'));
        $this->assertEquals('abc', EnumArrayDemo::get('ENUM2'));

        $this->assertTrue(EnumArrayDemo::isValidName('ENUM1'));
        $this->assertFalse(EnumArrayDemo::isValidName('enum1'));

        $this->assertEquals(['ENUM1' => 1, 'ENUM2' => 'abc'], EnumArrayDemo::getConstants(), true);

        $this->assertTrue(EnumArrayDemo::isValidValue(1));
        $this->assertTrue(EnumArrayDemo::isValidValue('abc'));
        $this->assertFalse(EnumArrayDemo::isValidValue('e3'));
    }

    /**
     * @expectedException \OutOfBoundsException
     */
    public function testInvalidConstantException()
    {
        EnumArrayDemo::get('enum2');
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Datatypes;

use phpOMS\Datatypes\Location;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class LocationTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $location = new Location();
        $this->assertObjectHasAttribute('postal', $location);
        $this->assertObjectHasAttribute('city', $location);
        $this->assertObjectHasAttribute('country', $location);
        $this->assertObjectHasAttribute('address', $location);
        $this->assertObjectHasAttribute('state', $location);
        $this->assertObjectHasAttribute('geo', $location);
    }

    public function testDefault()
    {
        $expected = [
            'postal'  => '',
            'city'    => '',
            'country' => '',
            'address' => '',
            'state'   => '',
            'geo'     => [
                'lat'  => 0,
                'long' => 0,
            ],
        ];

        $location = new Location();
        $this->assertEquals('', $location->getPostal());
        $this->assertEquals('', $location->getCity());
        $this->assertEquals('', $location->getCountry());
        $this->assertEquals('', $location->getAddress());
        $this->assertEquals('', $location->getState());
        $this->assertEquals(['lat' => 0, 'long' => 0], $location->getGeo());
        $this->assertEquals($expected, $location->toArray());
        $this->assertEquals(json_encode($expected), $location->toJson());
    }

    public function testGetSet()
    {
        $expected = [
            'postal'  => '0123456789',
            'city'    => 'city',
            'country' => 'Country',
            'address' => 'Some address here',
            'state'   => 'This is a state 123',
            'geo'     => [
                'lat'  => 12.1,
                'long' => 11.2,
            ],
        ];

        $location = new Location();

        $location->setPostal('0123456789');
        $location->setCity('city');
        $location->setCountry('Country');
        $location->setAddress('Some address here');
        $location->setState('This is a state 123');
        $location->setGeo(['lat'  => 12.1,
                           'long' => 11.2,]);

        $this->assertEquals('0123456789', $location->getPostal());
        $this->assertEquals('city', $location->getCity());
        $this->assertEquals('Country', $location->getCountry());
        $this->assertEquals('Some address here', $location->getAddress());
        $this->assertEquals('This is a state 123', $location->getState());
        $this->assertEquals(['lat' => 12.1, 'long' => 11.2], $location->getGeo());
        $this->assertEquals($expected, $location->toArray());
        $this->assertEquals(json_encode($expected), $location->toJson());
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Datatypes;

use phpOMS\Datatypes\Enum;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class EnumDemo extends Enum
{
    const ENUM1 = 1;
    const ENUM2 = ';l';
};

class EnumTest extends \PHPUnit_Framework_TestCase
{
    public function testGetSet()
    {
        $this->assertTrue(EnumDemo::isValidName('ENUM1'));
        $this->assertFalse(EnumDemo::isValidName('enum1'));

        $this->assertEquals(['ENUM1' => 1, 'ENUM2' => ';l'], EnumDemo::getConstants(), true);

        $this->assertTrue(EnumDemo::isValidValue(1));
        $this->assertTrue(EnumDemo::isValidValue(';l'));
        $this->assertFalse(EnumDemo::isValidValue('e3'));
    }
}

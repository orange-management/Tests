<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Datatypes;

use phpOMS\Datatypes\SmartDateTime;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class SmartDateTimeTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $datetime = new SmartDateTime();
        $this->assertInstanceOf('\DateTime', $datetime);
    }

    public function testGetSet()
    {
        $datetime = new SmartDateTime('1970-01-01');
        $this->assertEquals('1970-01-01', $datetime->format('Y-m-d'));

        $new = $datetime->createModify(1, 1, 1);
        $this->assertEquals('1970-01-01', $datetime->format('Y-m-d'));
        $this->assertEquals('1971-02-02', $new->format('Y-m-d'));

        $datetime = new SmartDateTime('1975-06-01');
        $this->assertEquals('1976-07-01', $datetime->createModify(0, 13)->format('Y-m-d'));
        $this->assertEquals('1976-01-01', $datetime->createModify(0, 7)->format('Y-m-d'));
        $this->assertEquals('1975-03-01', $datetime->createModify(0, -3)->format('Y-m-d'));
        $this->assertEquals('1974-11-01', $datetime->createModify(0, -7)->format('Y-m-d'));
        $this->assertEquals('1973-11-01', $datetime->createModify(0, -19)->format('Y-m-d'));
        $this->assertEquals('1973-12-01', $datetime->createModify(0, -19, 30)->format('Y-m-d'));
        $this->assertEquals('1973-12-31', $datetime->createModify(0, -18, 30)->format('Y-m-d'));
    }
}

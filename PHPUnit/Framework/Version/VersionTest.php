<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Version;

use phpOMS\Version\Version;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class VersionTest extends \PHPUnit_Framework_TestCase
{
    public function testVersionCompare()
    {
        $version1 = '1.23.456';
        $version2 = '1.23.567';

        $this->assertEquals(Version::compare($version1, $version1), 0);
        $this->assertEquals(Version::compare($version1, $version2), -1);
        $this->assertEquals(Version::compare($version2, $version1), 1);
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Utils\RnG;

use phpOMS\Utils\RnG\StringUtils;

require_once __DIR__ . '/../../../../../phpOMS/Autoloader.php';

class StringUtilsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @slowThreshold 1500
     */
    public function testStrings()
    {
        $haystack = [];
        $outOfBounds = false;
        $randomness = 0;

        for($i = 0; $i < 10000; $i++) {
            $random = StringUtils::generateString(5, 12, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_?><|;"');

            if(strlen($random) > 12 || strlen($random) < 5) {
                $outOfBounds = true;
            }

            if(in_array($random, $haystack)) {
                $randomness++;
            }

            $haystack[] = $random;
        }

        $this->assertFalse($outOfBounds);
        $this->assertLessThan(5, $randomness);
    }
}

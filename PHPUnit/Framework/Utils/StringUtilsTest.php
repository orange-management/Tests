<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Utils;

use phpOMS\Utils\StringUtils;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class StringUtilsTest extends \PHPUnit_Framework_TestCase
{
    public function testStrings()
    {
        $string = 'This is a test string.';
        $this->assertTrue(StringUtils::startsWith($string, 'This '));
        $this->assertFalse(StringUtils::startsWith($string, 'Thss '));
        $this->assertTrue(StringUtils::endsWith($string, 'string.'));
        $this->assertFalse(StringUtils::endsWith($string, 'strng.'));

        $this->assertTrue(StringUtils::mb_startsWith($string, 'This '));
        $this->assertFalse(StringUtils::mb_startsWith($string, 'Thss '));
        $this->assertTrue(StringUtils::mb_endsWith($string, 'string.'));
        $this->assertFalse(StringUtils::mb_endsWith($string, 'strng.'));

        $this->assertEquals('This ', StringUtils::mb_ucfirst('this '));
        $this->assertNotEquals('this ', StringUtils::mb_ucfirst('this '));
        $this->assertEquals('thss', StringUtils::mb_lcfirst('Thss'));
        $this->assertNotEquals('Thss', StringUtils::mb_lcfirst('Thss'));

        $this->assertEquals($string, StringUtils::mb_trim($string, ' '));
        $this->assertEquals('This is a test string', StringUtils::mb_trim($string, '.'));
        $this->assertEquals('asdf', StringUtils::mb_trim(' asdf ', ' '));
        $this->assertEquals('asdf', StringUtils::mb_trim('%asdf%', '%'));

        $this->assertEquals(' asdf', StringUtils::mb_rtrim(' asdf   '));
        $this->assertEquals('%asdf', StringUtils::mb_rtrim('%asdf%', '%'));

        $this->assertEquals('asdf  ', StringUtils::mb_ltrim(' asdf  '));
        $this->assertEquals('asdf%', StringUtils::mb_ltrim('%asdf%', '%'));
    }
}

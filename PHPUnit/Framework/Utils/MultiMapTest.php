<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Utils;

use phpOMS\Utils\MultiMap;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class MultiMapTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $map = new MultiMap();
        $this->assertInstanceOf('\phpOMS\Utils\MultiMap', $map);

        /* Testing members */
        $this->assertObjectHasAttribute('values', $map);
        $this->assertObjectHasAttribute('keys', $map);
    }

    public function testDefault()
    {
        $map = new MultiMap();

        /* Testing default values */
        $this->assertNull($map->get('someKey'));
        $this->assertEquals(0, $map->count());

        $this->assertEmpty($map->keys());
        $this->assertEmpty($map->values());
        $this->assertEmpty($map->getSiblings('someKey'));
        $this->assertFalse($map->removeKey('someKey'));
        $this->assertFalse($map->remap('old', 'new'));
        $this->assertFalse($map->remove('someKey'));
    }

    public function testSetGet()
    {
        $map = new MultiMap();

        $inserted = $map->add(['a', 'b'], 'val1');
        $this->assertEquals(1, $map->count());
        $this->assertTrue($inserted);
        $this->assertEquals('val1', $map->get('a'));
        $this->assertEquals('val1', $map->get('b'));

        $inserted = $map->add(['a', 'b'], 'val2');
        $this->assertEquals(1, $map->count());
        $this->assertTrue($inserted);
        $this->assertEquals('val2', $map->get('a'));
        $this->assertEquals('val2', $map->get('b'));

        $inserted = $map->add(['a', 'c'], 'val3', false);
        $this->assertEquals(2, $map->count());
        $this->assertTrue($inserted);
        $this->assertEquals('val2', $map->get('a'));
        $this->assertEquals('val2', $map->get('b'));
        $this->assertEquals('val3', $map->get('c'));

        $inserted = $map->add(['a', 'c'], 'val3', false);
        $this->assertEquals(2, $map->count());
        $this->assertFalse($inserted);
        $this->assertEquals('val2', $map->get('a'));
        $this->assertEquals('val2', $map->get('b'));
        $this->assertEquals('val3', $map->get('c'));

        $set = $map->set('d', 'val4');
        $this->assertFalse($set);
        $this->assertEquals(2, $map->count());

        $set = $map->set('b', 'val4');
        $this->assertEquals(2, $map->count());
        $this->assertTrue($set);
        $this->assertEquals('val4', $map->get('b'));
        $this->assertEquals('val4', $map->get('a'));
        $this->assertEquals('val3', $map->get('c'));

        $remap = $map->remap('b', 'd');
        $this->assertEquals(2, $map->count());
        $this->assertFalse($remap);

        $remap = $map->remap('d', 'b');
        $this->assertEquals(2, $map->count());
        $this->assertFalse($remap);

        $remap = $map->remap('d', 'e');
        $this->assertEquals(2, $map->count());
        $this->assertFalse($remap);

        $remap = $map->remap('b', 'c');
        $this->assertEquals(2, $map->count());
        $this->assertTrue($remap);
        $this->assertEquals('val3', $map->get('b'));
        $this->assertEquals('val4', $map->get('a'));
        $this->assertEquals('val3', $map->get('c'));

        $this->assertEquals(3, count($map->keys()));
        $this->assertEquals(2, count($map->values()));

        $this->assertTrue(is_array($map->keys()));
        $this->assertTrue(is_array($map->values()));

        $siblings = $map->getSiblings('d');
        $this->assertEmpty($siblings);

        $siblings = $map->getSiblings('a');
        $this->assertEquals(0, count($siblings));
        $this->assertEmpty($siblings);

        $siblings = $map->getSiblings('b');
        $this->assertEquals(1, count($siblings));
        $this->assertEquals(['c'], $siblings);

        $removed = $map->remove('d');
        $this->assertFalse($removed);

        $removed = $map->remove('d');
        $this->assertFalse($removed);

        $removed = $map->remove('c');
        $this->assertTrue($removed);
        $this->assertEquals(1, count($map->keys()));
        $this->assertEquals(1, count($map->values()));

        $removed = $map->removeKey('b');
        $this->assertFalse($removed);

        $removed = $map->removeKey('a');
        $this->assertTrue($removed);
        $this->assertEquals(0, count($map->keys()));
        $this->assertEquals(0, count($map->values()));
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Utils;

use phpOMS\Utils\ArrayUtils;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class ArrayUtilsTest extends \PHPUnit_Framework_TestCase
{
    public function testArrays()
    {
        $expected = [
            'a' => [
                'aa' => 1,
                'ab' => [
                    'aba',
                    'ab0',
                ],
            ],
            2 => '2a',
        ];

        $expected_str = "['a' => ['aa' => 1, 'ab' => [0 => 'aba', 1 => 'ab0', ], ], 2 => '2a', ]";

        $actual = [];

        $actual = ArrayUtils::setArray('a/aa', $actual, 1, '/');
        $actual = ArrayUtils::setArray('a/ab', $actual, ['aba'], '/');
        $actual = ArrayUtils::setArray('a/ab', $actual, 'abb', '/');
        $actual = ArrayUtils::setArray('2', $actual, '2a', '/');
        $actual = ArrayUtils::setArray('a/ab/1', $actual, 'ab0', '/', true);

        $this->assertEquals($expected, $actual);
        $this->assertTrue(ArrayUtils::inArrayRecursive('aba', $expected));
        $this->assertFalse(ArrayUtils::inArrayRecursive('aba', ArrayUtils::unsetArray('a/ab', $actual, '/')));
        $this->assertEquals($expected_str, ArrayUtils::stringify($expected));

        $this->assertEquals('2;3;1;"""Text;"' . "\n", ArrayUtils::arrayToCSV(['a' => 2, 3, 1, '"Text;'], ';', '"', '\\'));
    }
}

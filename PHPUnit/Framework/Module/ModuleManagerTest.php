<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Module;

use phpOMS\ApplicationAbstract;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Dispatcher\Dispatcher;
use phpOMS\Module\ModuleManager;
use phpOMS\Router\Router;
use phpOMS\Security\Encryption\Encryption;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class ModuleManagerTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;
    protected $app = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);

        $this->app = new class extends ApplicationAbstract {};
        $this->app->dbPool = $this->dbPool;
    }

    public function testAttributes()
    {
        $moduleManager = new ModuleManager($this->app);
        $this->assertInstanceOf('\phpOMS\Module\ModuleManager', $moduleManager);

        $this->assertObjectHasAttribute('running', $moduleManager);
        $this->assertObjectHasAttribute('installed', $moduleManager);
        $this->assertObjectHasAttribute('active', $moduleManager);
        $this->assertObjectHasAttribute('all', $moduleManager);
        $this->assertObjectHasAttribute('uriLoad', $moduleManager);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUnknownModuleInit()
    {
        $moduleManager = new ModuleManager($this->app);
        $moduleManager->initModule('doesNotExist');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUnknownModuleGet()
    {
        $moduleManager = new ModuleManager($this->app);
        $moduleManager->get('doesNotExist');
    }

    public function testGetSet()
    {
        $this->app->router = new Router();
        $this->app->dispatcher = new Dispatcher($this->app);

        $moduleManager = new ModuleManager($this->app);

        $active    = $moduleManager->getActiveModules();
        $all       = $moduleManager->getAllModules();
        $installed = $moduleManager->getInstalledModules();

        $this->assertNotEmpty($active);
        $this->assertNotEmpty($all);
        $this->assertNotEmpty($installed);

        $this->assertInstanceOf('\phpOMS\Module\ModuleAbstract', $moduleManager->get('Admin'));
        $this->assertInstanceOf('\Modules\Admin\Controller', $moduleManager->get('Admin'));
    }
}

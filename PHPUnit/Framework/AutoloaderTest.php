<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework;

require_once __DIR__ . '/../../../phpOMS/Autoloader.php';
use phpOMS\Autoloader;

class AutoloaderTest extends \PHPUnit_Framework_TestCase
{
    public function testAutoloader()
    {
        $this->assertTrue(is_string(Autoloader::exists('\phpOMS\Autoloader')));
        $this->assertFalse(Autoloader::exists('\Does\Not\Exist'));
    }
}

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Framework\Log;

use phpOMS\Log\FileLogger;
use phpOMS\Log\LogLevel;

require_once __DIR__ . '/../../../../phpOMS/Autoloader.php';

class FileLoggerTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributes()
    {
        $log = FileLogger::getInstance(__DIR__);
        $this->assertObjectHasAttribute('fp', $log);
        $this->assertObjectHasAttribute('path', $log);
    }

    public function testDefault()
    {
        $log = FileLogger::getInstance(__DIR__);
        $this->assertEquals([], $log->countLogs());
        $this->assertEquals([], $log->getHighestPerpetrator());
        $this->assertEquals([], $log->get());
        $this->assertEquals([], $log->getByLine());
    }

    public function testGetSet()
    {
        $log = new FileLogger(__DIR__ . '/test.log');

        $log->emergency(FileLogger::MSG_FULL, [
            'message' => 'msg',
            'line'    => 11,
            'file'    => FileLoggerTest::class,
        ]);

        $log->alert(FileLogger::MSG_FULL, [
            'message' => 'msg',
            'line'    => 11,
            'file'    => FileLoggerTest::class,
        ]);

        $log->critical(FileLogger::MSG_FULL, [
            'message' => 'msg',
            'line'    => 11,
            'file'    => FileLoggerTest::class,
        ]);

        $log->error(FileLogger::MSG_FULL, [
            'message' => 'msg',
            'line'    => 11,
            'file'    => FileLoggerTest::class,
        ]);

        $log->warning(FileLogger::MSG_FULL, [
            'message' => 'msg',
            'line'    => 11,
            'file'    => FileLoggerTest::class,
        ]);

        $log->notice(FileLogger::MSG_FULL, [
            'message' => 'msg',
            'line'    => 11,
            'file'    => FileLoggerTest::class,
        ]);

        $log->info(FileLogger::MSG_FULL, [
            'message' => 'msg',
            'line'    => 11,
            'file'    => FileLoggerTest::class,
        ]);

        $log->debug(FileLogger::MSG_FULL, [
            'message' => 'msg',
            'line'    => 11,
            'file'    => FileLoggerTest::class,
        ]);

        $log->log(LogLevel::DEBUG, FileLogger::MSG_FULL, [
            'message' => 'msg',
            'line'    => 11,
            'file'    => FileLoggerTest::class,
        ]);

        $this->assertEquals(1, $log->countLogs()['emergency']);
        $this->assertEquals(1, $log->countLogs()['alert']);
        $this->assertEquals(1, $log->countLogs()['critical']);
        $this->assertEquals(1, $log->countLogs()['error']);
        $this->assertEquals(1, $log->countLogs()['warning']);
        $this->assertEquals(1, $log->countLogs()['notice']);
        $this->assertEquals(1, $log->countLogs()['info']);
        $this->assertEquals(2, $log->countLogs()['debug']);

        $this->assertEquals(['0.0.0.0' => 9], $log->getHighestPerpetrator());
        $this->assertEquals([5, 6, 7, 8, 9], array_keys($log->get(5, 1)));
        $this->assertEquals('alert', $log->getByLine(2)['level']);

        unlink(__DIR__ . '/test.log');
        unlink(__DIR__ . '/' . date('Y-m-d') . '.log');
    }
}

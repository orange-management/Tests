<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */

namespace Tests\PHPUnit\Install;

use Install\Installer;
use phpOMS\ApplicationAbstract;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\Module\ModuleManager;

require_once __DIR__ . '/../../../phpOMS/Autoloader.php';
require_once __DIR__ . '/../../../config.php';

class InstallTest extends \PHPUnit_Framework_TestCase
{
    protected $dbPool = null;

    protected function setUp()
    {
        $this->dbPool = new Pool();
        /** @var array $CONFIG */
        $this->dbPool->create('core', $GLOBALS['CONFIG']['db']['core']['masters'][0]);
    }

    /**
     * @group admin
     */
    public function testInstall()
    {
        // Getting all modules
        $toInstall = [
            'Admin',
        ];

        $instHOBJ = new Installer($this->dbPool);

        $result = $instHOBJ->installCore();
        $this->assertTrue($result);

        $result = $instHOBJ->installModules($toInstall);
        $this->assertTrue($result);

        $result = $instHOBJ->installGroups();
        $this->assertTrue($result);

        $result = $instHOBJ->installUsers();
        $this->assertTrue($result);

        $result = $instHOBJ->installSettings();
        $this->assertTrue($result);

        // todo: implement schema query builder so it's possible to check if table exists. maybe performing a unit test
        // here on the schema is wrong after all and should be done only in the modules after the framework test
    }
}

<?php

ini_set('memory_limit', '512M');

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../../phpOMS/Autoloader.php';
use phpOMS\DataStorage\Session\HttpSession;

$httpSession = new HttpSession();
